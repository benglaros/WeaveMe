//
//  WeftPickShuttleView.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/4/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

@IBDesignable
class WeftPickShuttleView: UIView {

    @IBOutlet weak var threadBackgroundView: UIView!
    @IBOutlet weak var threadColorView: UIView!
    @IBOutlet weak var shuttleImageView: UIImageView!
    
    var weftPick: WeftPick? {
        didSet {
            guard let weftPick = weftPick else {
                return
            }
            threadColorView.backgroundColor = weftPick.threadColor
            if weftPick.threadDiameter == 1.0 {
                threadColorView.transform = .identity
            } else {
                threadColorView.transform = CGAffineTransform(scaleX: 1.0, y: weftPick.threadDiameter)
            }

            // For bright/dark we use a gray background
            if weftPick.threadColor.hsv.value < 0.2 || weftPick.threadColor.hsv.value > 0.8 {
                threadBackgroundView.backgroundColor = .gray
            } else if weftPick.threadColor.hsv.value > 0.5 {
                threadBackgroundView.backgroundColor = .black
            } else {
                threadBackgroundView.backgroundColor = .white
            }
            if oldValue?.direction != weftPick.direction || oldValue?.leverType != weftPick.leverType {
                updateShuttleImage(direction: weftPick.direction, leverType: weftPick.leverType)
            }
        }
    }
    
    private func updateShuttleImage(direction: PickDirection, leverType: WeftPick.LeverType) {
        if leverType == .positive {
            if direction == .leftToRight {
                shuttleImageView.image = UIImage.init(named: "RightPlusShuttle")
            } else {
                shuttleImageView.image = UIImage.init(named: "LeftPlusShuttle")
            }
        } else {
            if direction == .leftToRight {
                shuttleImageView.image = UIImage.init(named: "RightMinusShuttle")
            } else {
                shuttleImageView.image = UIImage.init(named: "LeftMinusShuttle")
            }
        }
    }

    override func awakeFromNib() {
        // This method is called at runtime
        super.awakeFromNib()
        commonAwake()
    }
    
    override func prepareForInterfaceBuilder() {
        // Called by Interface Builder, but not at runtime.
        super.prepareForInterfaceBuilder()
        commonAwake()
    }
    
    class func instantiate() -> WeftPickShuttleView {
        guard let shuttleView = Bundle.main.loadNibNamed("WeftPickShuttleView", owner: nil, options: nil)?.first as?  WeftPickShuttleView else {
            fatalError()
        }
        return shuttleView
    }
    
    private func commonAwake() {

    }
    
}
