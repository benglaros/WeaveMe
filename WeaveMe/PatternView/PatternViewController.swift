//
//  PatternViewController.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/17/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is the viewcontroller that lets you step though a pattern and send individual picks to
//  the loom.  It contains a scrollViewController that displays the pattern in a sort of expanded schematic view.
//

import UIKit

class PatternViewController: UIViewController, KSBluetoothManagerStateListener {
   
    var pattern: Pattern? {
        didSet {
            pickSequencer.pattern = pattern
        }
    }
    
    private var patternScrollViewController: PatternScrollViewController?
    private let pickSequencer = PickSequencer()
    private let shuttleController = ShuttleController()
    private lazy var connectionController = ConnectionStatusController(buttonItem: connectBarButtonItem)
    
    @IBOutlet weak var connectBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var loomSendingEnabledSwitch: UISwitch!
    @IBOutlet weak var homeButton: UIButton!
    
    @IBOutlet weak var arrowUpButton: UIButton!
    @IBOutlet weak var weftIndexLabel: UILabel!
    @IBOutlet weak var arrowDownButton: UIButton!

    var loomIsUpdatingCams = false {
        didSet {
            if oldValue != loomIsUpdatingCams {
                arrowUpButton.isEnabled = pickSequencer.upShouldBeEnabled && !loomIsUpdatingCams
                arrowDownButton.isEnabled = pickSequencer.downShouldBeEnabled && !loomIsUpdatingCams
            }
        }
    }

    private func updateSequenceUI() {
        arrowUpButton.isEnabled = pickSequencer.upShouldBeEnabled && !loomIsUpdatingCams
        arrowDownButton.isEnabled = pickSequencer.downShouldBeEnabled && !loomIsUpdatingCams
        weftIndexLabel.text = pickSequencer.currentPositionName
        
        patternScrollViewController?.unhighlightAll(animated: true) { finished in
            if let currentPick = self.pickSequencer.currentWeftPick {
                self.patternScrollViewController?.highlightPick(currentPick, animated: true) { finished in
                }
            }
        }
        
        // TODO: Move this into a helper class so it's on our view instead of inside the scroll view.
        shuttleController.hideShuttle(animated: true) { finished in
            if let currentPick = self.pickSequencer.currentWeftPick {
// Disable this for tonight.  It needs more testing and with the tabby it's ineffective anyway.
//                if let optimizedLeverType = Loom.shared.getLeverTypeForOptimizedCamMotion(to: currentPick) {
//                    currentPick.leverType = optimizedLeverType
//                }
                self.shuttleController.showShuttle(for: currentPick, animated: true) { finished in
                }
            }
        }
    }
    
    var lastConfirmedWeftPick: WeftPick?
    var mostRecentConnectionState: KSPeripheralConnectionState = .notConnected
    
    var currentImage: UIImage?
    var loomWarpLayerCount: Int?
    var maxColumns: Int? {
        if let layerCount = loomWarpLayerCount {
            if let image = currentImage {
                return min(layerCount, Int(image.size.width))
            } else {
                return layerCount
            }
        } else {
            if let image = currentImage {
                return Int(image.size.width)
            }
        }
        return nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        KSBluetoothManager.shared.addStateChangeListener(listener: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private var isFirstViewDidAppear = true
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstViewDidAppear {
            isFirstViewDidAppear = false
            self.loomSendingEnabledSwitch.setOn(false, animated: false)
            updateWithConnectionState(KSBluetoothManager.shared.connectionState)
            updateSequenceUI()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func patternHeight() -> Int {
        return pattern?.threadCount(.weft) ?? 0
    }
    
    // This fishes the current weft pick out of the sequencer and sends it to the loom.
    private func sendCurrentWeftPickToLoom(completion: @escaping (_ success: Bool) -> Void) {
        guard let weftPick = pickSequencer.currentWeftPick else {
            completion(true)
            return
        }
        
        // If the pick has been changed since the last one we sent we fire it off.
        if lastConfirmedWeftPick != weftPick {
// Currently disabled util this can be shown to be useful, and it is fully debugged.
//            if let optimizedLever = Loom.shared.getLeverTypeForOptimizedCamMotion(to: weftPick) {
//                weftPick.leverType = optimizedLever
//            }

            Loom.shared.sendWeftPickToLoom(weftPick) { success, error in
                if success {
                    self.lastConfirmedWeftPick = weftPick
                    self.loomIsUpdatingCams = true
                }
                completion(success)
            }
        }
    }
    
    @IBAction func loomSendingSwitchValueChanged(_ sender: Any) {
        // TODO: We really should have some sort of connection indicator.
        homeButton.isEnabled = KSBluetoothManager.shared.connectionState == .connected && loomSendingEnabledSwitch.isOn
        
        if loomSendingEnabledSwitch.isOn {
            sendCurrentWeftPickToLoom() { success in }
        }
    }
    
    @IBAction func arrowUp(_ sender: Any) {
        if !pickSequencer.upShouldBeEnabled {
            return
        }
        pickSequencer.moveUp()
        updateSequenceUI()
        
        // If we're sending, send it.  Otherwise just let them click around.
        if loomSendingEnabledSwitch.isOn {
            sendCurrentWeftPickToLoom() { success in }
        }
    }
    
    @IBAction func arrowDown(_ sender: Any) {
        if !pickSequencer.downShouldBeEnabled {
            return
        }
        
        pickSequencer.moveDown()
        updateSequenceUI()
        
        // If we're sending, send it.  Otherwise just let them click around.
        if loomSendingEnabledSwitch.isOn {
            sendCurrentWeftPickToLoom() { success in }
        }
    }
    
    @IBAction func homeTapped(_ sender: Any) {
        Loom.shared.prepareForShutdown() { success, error in
            self.loomSendingEnabledSwitch.isOn = false
        }
    }
    
    private func updateWithConnectionState(_ connectionState: KSPeripheralConnectionState) {
        if connectionState != .connected && loomSendingEnabledSwitch.isOn {
            loomSendingEnabledSwitch.setOn(false, animated: true)
        }
        // If we get dissconnected we won't hear back from the loom about cam state updates so we clear this out.
        if connectionState != .connected {
            loomIsUpdatingCams = false
        }
        loomSendingEnabledSwitch.isEnabled = connectionState == .connected
        homeButton.isEnabled = connectionState == .connected && loomSendingEnabledSwitch.isOn

    }
    
    // MARK: - KSBluetoothManagerStateListener
    func connectionStateChanged(_ connectionState: KSPeripheralConnectionState) {
        mostRecentConnectionState = connectionState
        
        // If we've never asked the loom about the number of layers we do so as soon as we connect.
        if loomWarpLayerCount == nil {
            Loom.shared.getLoomWarpCount { (count, error) in
                self.loomWarpLayerCount = count
            }
        }
        
        if self.view == nil {
            return
        }
        updateWithConnectionState(connectionState)
    }
    
    override func viewDidLayoutSubviews() {
        guard let scrollView = self.patternScrollViewController?.view else {
            return
        }
        let shuttleCenter = scrollView.convert(CGPoint(x: scrollView.bounds.size.width/2.0, y: 0.0), to: self.view)
        shuttleController.centerX = shuttleCenter.x
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = pattern?.name ?? "Unknown"
        patternScrollViewController?.pattern = pattern
        
        shuttleController.view = view
        
        connectionController.delegate = self
        updateWithConnectionState(mostRecentConnectionState)

        KSBluetoothManager.shared.addCommandListener(listener: self, for: "nc", withDescription: "New Cam Positions Available")
    }
    
    class func instantiate() -> PatternViewController {
        let storyboard = UIStoryboard(name: "PatternViewController", bundle: nil)
        guard let patternViewController = storyboard.instantiateViewController(withIdentifier: "PatternViewController") as? PatternViewController else {
            fatalError()
        }
        return patternViewController
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "patternScrollEmbed" {
            patternScrollViewController = segue.destination as? PatternScrollViewController
            patternScrollViewController?.pattern = pattern
            patternScrollViewController?.delegate = self
        }
    }

}

extension PatternViewController: PatternScrollViewControllerDelegate {
    // MARK: - PatternScrollViewControllerDelegate
    func patternScrollViewController(_ scrollViewController: PatternScrollViewController, didTapOnPatternPoint point: CGPoint) {
        // For now we don't let you tap around when you're sending to the loom to avoid a miss tap.
        if loomSendingEnabledSwitch.isOn {
            return
        }
        pickSequencer.setCurrentY(Int(point.y))
        updateSequenceUI()
    }
}

extension PatternViewController: ConnectionStatusControllerDelegate {
    // MARK: - ConnectionStatusControllerDelegate
    func connectionStatusController(_ connectionStatusController: ConnectionStatusController, didChangeStatusText titleText: String) {
        title = titleText
    }
    
}

extension PatternViewController: KSBluetoothManagerCommandListener {

    func didReceiveCommand(_ command: String, argument: String) {
        if command == "nc" {
            // There is a race condition here where the loom will be asking via BT about all the new cam positions.
            loomIsUpdatingCams =  false
        }
    }

}
