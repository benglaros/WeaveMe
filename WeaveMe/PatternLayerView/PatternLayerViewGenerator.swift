//
//  PatternLayerViewGenerator.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 7/5/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This is just a helper class that can break a given Pattern into a set of PatternLayerImageViews which can then
//  be used to render/display the pattern.  For ground layers this is quick to compute since those layers
//  are entirely filled and the views are just breaking down the height into chunks <= maxPatternHeight.
//  For overlays this can be somewhat expensive to do, since it since it has to build the picks for the PatternLayer
//  in order to figure out which parts of the overlay actually have thread. If you're going to build the picks anyway
//  or have already built the picks that's not a big deal.
//
//  We don't do anything super complicated to figure out the min bounding boxes since we know how brocade overlays
//  tend to be organized.

import UIKit

class PatternLayerViewGenerator {

    // The max weft rows a view can span.  Taller non empty regions are cut apart into bands this size.
    var maxPatternHeight = 40

    /// This figures out the regions associated with a given pattern layer.  These regions can then be rendered
    /// and displayed.  This segmentation into regions helps with scrolling displays and for overlays that take up
    /// very little space.
    ///
    /// - Parameters:
    ///   - pattern: The layer to build the the views for.
    ///   - targetOutputWidth: The output width for the entire patternLayer, not for the individual parts.
    /// - Returns: A list of PatternLayerViews that covers the entire pattern.  The ordering is important
    ///            because they reflect are ordered from bottom to top, so they can be added as subviews
    ///            in the given order.
    func buildPatternLayerViews(for pattern: Pattern, targetOutputWidth: CGFloat) -> [PatternLayerView] {
        var outputViews: [PatternLayerView] = []
        for patternLayer in pattern.patternLayers {
            if patternLayer.layerType == .ground {
                outputViews.append(contentsOf: buildGroundLayerViews(for: patternLayer, targetOutputWidth: targetOutputWidth))
            } else if patternLayer.layerType == .overlay {
                outputViews.append(contentsOf: buildOverlayViews(for: patternLayer, targetOutputWidth: targetOutputWidth))
            }
        }
        return outputViews
    }

    private func getPatternRect(minX: Int, maxX: Int, minY: Int, maxY: Int) -> CGRect {
        return CGRect(x: minX, y: minY, width: (maxX - minX) + 1, height: (maxY - minY) + 1)
    }

    // This computes the frame from the ranges, and builds a PatternLayerView from that.
    private func buildPatternLayerView(for patternLayer: PatternLayer, patternRect: CGRect, tileSize: CGSize) -> PatternLayerView {

        let frame = CGRect(x: patternRect.minX*tileSize.width, y: patternRect.minY*tileSize.height,
                           width: patternRect.width*tileSize.width, height: patternRect.height*tileSize.height)
        let style: PatternRenderer.RenderStyle = patternLayer.layerType == .overlay ? .normalOnTopWeft : .normalWarpAndWeft

        return PatternLayerView(frame: frame, patternLayer: patternLayer, renderSettings: RenderSettings(rect: patternRect, style: style, outputImageWidth: frame.width))
    }

    // Ground regions always stretch across the full X range.
    private func buildGroundLayerViews(for patternLayer: PatternLayer, targetOutputWidth: CGFloat) -> [PatternLayerView] {
        let width = patternLayer.threadCount(.warp)
        let height = patternLayer.threadCount(.weft)
        if width == 0 || height == 0 {
            return []
        }

        let tileWidth = targetOutputWidth/CGFloat(width)
        let tileSize = CGSize(width: tileWidth, height: tileWidth)

        var patternLayerViews: [PatternLayerView] = []

        // There are no gaps in a ground layer, so we only chop this up to maintain the max height.
        var currentY = 0
        while currentY + (maxPatternHeight - 1) < height {
            patternLayerViews.append(buildPatternLayerView(for: patternLayer,
                                                           patternRect: getPatternRect(minX: 0, maxX: width - 1, minY: currentY, maxY: currentY + (maxPatternHeight - 1)),
                                                           tileSize: tileSize))
            currentY += maxPatternHeight
        }

        // If there is still any Y unaccounted for we put it in a final region.
        if currentY <= height - 1 {
            patternLayerViews.append(buildPatternLayerView(for: patternLayer,
                                                           patternRect: getPatternRect(minX: 0, maxX: width - 1, minY: currentY, maxY: height - 1),
                                                           tileSize: tileSize))
        }

        return patternLayerViews
    }

    // We merge pattern layer views if the gaps between their pattern rects are <= maxGap and the
    // merged area does not get taller than maxPatternHeight.  That makes it so if overlays are
    // splitting into single line pieces because it's being mixed with plain weave we can merge them.
    // We assume the input is non overlapping in Y and that it is in assending y order.
    private func mergeInY(_ patternLayerViews: [PatternLayerView], tileSize: CGSize, maxGap: Int) -> [PatternLayerView] {
        guard let  firstView = patternLayerViews.first else {
            return patternLayerViews
        }

        var outputPatternLayerViews: [PatternLayerView] = []
        var currentPatternRect: CGRect?

        for view in patternLayerViews {
            let rect = view.patternRenderSettings.rect
            if let rectThusFar = currentPatternRect {
                if rect.minY <= rectThusFar.maxY + CGFloat(maxGap) && rectThusFar.union(rect).height <= CGFloat(maxPatternHeight) {
                    currentPatternRect = rectThusFar.union(rect)
                } else {
                    outputPatternLayerViews.append(buildPatternLayerView(for: firstView.patternLayer, patternRect: rectThusFar, tileSize: tileSize))
                    currentPatternRect = rect
                }
            } else {
                currentPatternRect = rect
            }
        }
        if let rectThusFar = currentPatternRect {
            outputPatternLayerViews.append(buildPatternLayerView(for: firstView.patternLayer, patternRect: rectThusFar, tileSize: tileSize))
        }

        return outputPatternLayerViews
    }

    private func buildOverlayViews(for patternLayer: PatternLayer, targetOutputWidth: CGFloat) -> [PatternLayerView] {
        let width = patternLayer.threadCount(.warp)
        let height = patternLayer.threadCount(.weft)

        var patternLayerViews: [PatternLayerView] = []

        let tileWidth = targetOutputWidth/CGFloat(width)
        let tileSize = CGSize(width: tileWidth, height: tileWidth)

        var minXInCurrentRange: Int?
        var maxXInCurrentRange: Int?
        var minYInCurrentRange = 0

        for y in 0..<height {
            let weftPicks = patternLayer.weftPicksForY(y)

            if let weftPick = weftPicks.first {
                // If we're starting a new range nail down the y.
                if minXInCurrentRange == nil {
                    minYInCurrentRange = y
                }
                minXInCurrentRange = min(minXInCurrentRange ?? width, weftPick.minX)
                maxXInCurrentRange = max(maxXInCurrentRange ?? 0, weftPick.maxX)

                // If this range has gotten too tall we also generate a region.
                if y > minYInCurrentRange + maxPatternHeight,
                    let minX = minXInCurrentRange,
                    let maxX = maxXInCurrentRange {
                    let minY = minYInCurrentRange
                    let maxY = y
                    patternLayerViews.append(buildPatternLayerView(for: patternLayer,
                                                                   patternRect: getPatternRect(minX: minX, maxX: maxX, minY: minY, maxY: maxY),
                                                                   tileSize: tileSize))
                    minXInCurrentRange = nil
                    maxXInCurrentRange = nil
                    minYInCurrentRange = y
                }

            } else {
                // If we have nothing on this row, we finish any region we have
                // already started.
                if let minX = minXInCurrentRange,
                    let maxX = maxXInCurrentRange {
                    let minY = minYInCurrentRange
                    let maxY = y - 1
                    patternLayerViews.append(buildPatternLayerView(for: patternLayer,
                                                                   patternRect: getPatternRect(minX: minX, maxX: maxX, minY: minY, maxY: maxY),
                                                                   tileSize: tileSize))
                    minXInCurrentRange = nil
                    maxXInCurrentRange = nil
                    minYInCurrentRange = y
                }
            }
        }

        // We may have just produced a bunch of single line views because mixing plain weave in can inject
        // blank lines onto the overlays, so we merge those.
        return mergeInY(patternLayerViews, tileSize: tileSize, maxGap: 2)
    }

}
