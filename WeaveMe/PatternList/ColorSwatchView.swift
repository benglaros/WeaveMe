//
//  ColorSwatchView.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/13/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

class ColorSwatchView: UIView {
    var colors: [UIColor] = [] {
        didSet {
            setNeedsLayout()
            swatchesAreOutOfSync = true
        }
    }
    
    private var colorSwatchViews: [UIView] = []
    private let colorSwatchLabel = UILabel()
    private var swatchesAreOutOfSync = true
    
    private var colorSwatchSize: CGFloat = 20.0
    private var colorSwatchSpacing: CGFloat = 8.0
    private var colorSwatchCornerRadius: CGFloat = 4
    private var colorSwatchBorderWidth: CGFloat = 2
    
    // This x is the left most point where the label should go,
    // the y is where it's center should go.
    private func updateColorSwatchLabel(count: Int, x: CGFloat, y: CGFloat) {
        if colorSwatchLabel.superview != self {
            colorSwatchLabel.backgroundColor = UIColor.white
            colorSwatchLabel.layer.borderColor = UIColor.black.cgColor
            colorSwatchLabel.layer.borderWidth = colorSwatchBorderWidth
            colorSwatchLabel.layer.cornerRadius = colorSwatchCornerRadius
        }

        colorSwatchLabel.text = "+\(count)"
        colorSwatchLabel.sizeToFit()
        
        colorSwatchLabel.bounds = CGRect.init(x: 0, y: 0,
                                              width: max(colorSwatchLabel.bounds.size.width, colorSwatchSize),
                                              height: max(colorSwatchLabel.bounds.size.height, colorSwatchSize))

        colorSwatchLabel.center = CGPoint.init(x: x + colorSwatchLabel.bounds.size.width/2.0, y: y)
        addSubview(colorSwatchLabel)
    }
    
    // Layout some color swatch views, plus possible a number at the left hand side which says how many other swatches didn't fit.
    private func updateSwatchViews() {
        // TODO: Eventually we should support a [] [] [] +4 style if there are 4 more colors than swatches that can fit.
        
        // If there are more swatches we should show a +4 style swatch, so we leave one swatches space for that.
        var maxSwatchCount = Int(floor(bounds.size.width / (colorSwatchSize + colorSwatchSpacing))) - 2
        if maxSwatchCount < 0 {
            maxSwatchCount = 0
        }
        
        let swatchCount = min(colors.count, maxSwatchCount)
        
        // First make sure we have the correct number of swatches
        while colorSwatchViews.count > swatchCount {
            colorSwatchViews.last?.removeFromSuperview()
            colorSwatchViews.removeLast()
        }
        
        while colorSwatchViews.count < swatchCount {
            let swatchView = UIView()
            
            swatchView.bounds = CGRect.init(x: 0, y: 0, width: colorSwatchSize, height: colorSwatchSize)
            swatchView.layer.borderColor = UIColor.black.cgColor
            swatchView.layer.borderWidth = colorSwatchBorderWidth
            swatchView.layer.cornerRadius = colorSwatchCornerRadius
            
            colorSwatchViews.append(swatchView)
            self.addSubview(swatchView)
        }

        var x: CGFloat = colorSwatchSize/2.0
        let y: CGFloat = colorSwatchSize/2.0
        var index = 0
        for swatch in colorSwatchViews {
            swatch.center = CGPoint.init(x: x, y: y)
            swatch.backgroundColor = colors[index]
            x += colorSwatchSize + colorSwatchSpacing
            index += 1
        }
        
        if swatchCount < colors.count {
            updateColorSwatchLabel(count: colors.count - swatchCount, x: x - colorSwatchSize/2, y: y)
        } else {
            colorSwatchLabel.removeFromSuperview()
            colorSwatchLabel.text = ""
        }
        
        swatchesAreOutOfSync = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if swatchesAreOutOfSync {
            updateSwatchViews()
        }
    }
    
}
