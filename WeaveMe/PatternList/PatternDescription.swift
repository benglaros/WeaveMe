//
//  PatternDescription.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 6/15/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This is just an easy way to build the hard coded list of patterns shown in the list view.
//  

import UIKit

struct PatternDescription {
    // A unique id that we might eventually use for state restoration.
    var uuid: String

    // What's displayed is the list view.
    var name: String

    // Ordered list of image names with .first being the ground layer and each one above being an overlay layer.
    var imageNames: [String]

    var warpRepeats: Int?
    var weftRepeats: Int?
    var defaultWarpThreadDiameter: CGFloat?

    // If non nil the builder will alternate plain weave between rows of the pattern using a pattern mixing layer.
    var tabbyWeftColor: UIColor?

    // If non nil the builder will use the pattern crop layer to add that amount of plain weave on either side of the pattern
    // on non overlay layers.
    var plainWeaveLeftRightEdgeInset: Int?

    init(uuid: String, name: String, imageNames: [String], warpRepeats: Int? = nil, weftRepeats: Int? = nil, defaultWarpThreadDiameter: CGFloat? = nil,
         tabbyWeftColorHexString: String? = nil, plainWeaveLeftRightEdgeInset: Int? = nil) {
        self.uuid = uuid
        self.name = name
        self.imageNames = imageNames
        self.warpRepeats = warpRepeats
        self.weftRepeats = weftRepeats
        self.defaultWarpThreadDiameter = defaultWarpThreadDiameter
        if let hexColor = tabbyWeftColorHexString {
            self.tabbyWeftColor =  UIColor(hexString: hexColor)
        }
        self.plainWeaveLeftRightEdgeInset = plainWeaveLeftRightEdgeInset
    }
}
