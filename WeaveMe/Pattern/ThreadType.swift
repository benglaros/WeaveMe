//
//  ThreadType.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/10/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import Foundation

enum ThreadType: Int, CustomStringConvertible {
    case warp = 0
    case weft = 1

    var description: String {
        if self == .warp {
            return "warp"
        }
        return "weft"
    }
}
