//
//  PatternLayerCache.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 7/7/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  Because we use cropping/mixing pattern layers in a hierarchy and many of the lookups are used in the main
//  rendering loop, we use this cache structure to control the info we precompute since currently all these things
//  are very static.
//

import UIKit

class PatternLayerCache {
    var warpItems: [PatternLayerCachItem]?
    var weftItems: [PatternLayerCachItem]?
    var warpCount: Int?
    var weftCount: Int?

    func buildCache(for patternLayer: PatternLayer) {
        let warpCount = patternLayer.threadCount(.warp)
        self.warpCount = warpCount
        let weftCount = patternLayer.threadCount(.weft)
        self.weftCount = weftCount

        if warpCount < 1 || weftCount < 1 {
            warpItems = nil
            weftItems = nil
            return
        }

        var newWarpItems: [PatternLayerCachItem] = []
        newWarpItems.reserveCapacity(warpCount)
        for x in 0..<warpCount {
            newWarpItems.append(PatternLayerCachItem(color: patternLayer.layerType == .ground ? patternLayer.warpColor(x: x) : nil,
                                                     threadDiameter: patternLayer.threadDiameter(.warp, x: x, y: 0)))
        }
        warpItems = newWarpItems

        var newWeftItems: [PatternLayerCachItem] = []
        newWeftItems.reserveCapacity(weftCount)
        for y in 0..<weftCount {
            // Do not record the weft color for overlays since that can be clear for some x values and that matters.
            newWeftItems.append(PatternLayerCachItem(color: patternLayer.layerType == .ground ? patternLayer.weftColor(y: y) : nil,
                                                     threadDiameter: patternLayer.threadDiameter(.weft, x: 0, y: y)))
        }
        weftItems = newWeftItems
    }

    func flushCache() {
        warpItems = nil
        weftItems = nil
        warpCount = nil
        weftCount = nil
    }

    func threadCount(_ threadType: ThreadType) -> Int? {
        if threadType == .warp {
            return warpCount
        }
        return weftCount
    }

    func threadColor(_ threadType: ThreadType, x: Int, y: Int) -> UIColor? {
        if threadType == .warp {
            if let cacheItems = warpItems {
                return cacheItems[x].color
            }
        } else {
            if let cacheItems = weftItems {
                return cacheItems[y].color
            }
        }
        return nil
    }

    func threadDiameter(_ threadType: ThreadType, x: Int, y: Int) -> CGFloat? {
        if threadType == .warp {
            if let cacheItems = warpItems,
                let threadDiameter = cacheItems[x].threadDiameter {
                return threadDiameter
            }
        } else {
            if let cacheItems = weftItems,
                let threadDiameter = cacheItems[y].threadDiameter {
                return threadDiameter
            }
        }
        return nil
    }
}
