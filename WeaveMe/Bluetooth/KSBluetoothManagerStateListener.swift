//
//  KSBluetoothManagerStateListener.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 5/10/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  Listeners to the bluetooth object get informed about connection state changes
//

import UIKit

@objc protocol KSBluetoothManagerStateListener: class {

    // Called any time the connection state changes.
    func connectionStateChanged(_ connectionState: KSPeripheralConnectionState)
}
