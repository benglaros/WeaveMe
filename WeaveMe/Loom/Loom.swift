//
//  Loom.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/10/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is a loom specific interface that wraps various bluetooth communications with the
//  loom device.  It also keeps track of state/information specific to the loom like
//  number of cams the loom has, and the state of all the cams.
//

import UIKit

class Loom {
    
    static let shared = Loom()
    private init() {
        KSBluetoothManager.shared.addStateChangeListener(listener: self)
        KSBluetoothManager.shared.addCommandListener(listener: self, for: "nc", withDescription: "New Cam Positions Available")
    }

    // This is used to limit the number of cam positions sent/gotten from the loom.
    // It's not something that dynamically changes so we require getting it once and
    // then never bother to ask the loom about it again.
    private var loomWarpCount: Int?
    let defaultWarpCount = 60
    
    /// Ask the loom how many  warp layers it supports.
    func getLoomWarpCount(completion: @escaping (_ count: Int?, _ error: Error?) -> Void) {
        if let count = loomWarpCount {
            completion(count, nil)
            return
        }
        KSBluetoothManager.shared.sendCommand("gn", annotation: "Get Number of Warp Layers",
                                              success: { response in
                                                if let responseString = response,
                                                    let value = Int(responseString) {
                                                    self.loomWarpCount = value
                                                    completion(value, nil)
                                                }
        }, failure: { response in completion(nil, self.failureError())}, finally: {})
    }

    // The warp layer states start out as unknown.  After the loom does an IR scan of
    // the states we can read them in and after that we use those cached states internally to
    // compute/update the next set of cam positions and weather or not to use the + or - lever.
    // If something fails then it's best to rescan and start again.
    // start again.
    private var warpState: WarpState?

    // Warp layers states come back in greatest to least layer index, so we reverse them.
    // This is mostly for visual debugging since the printed order matches what you see looking at the loom from the front.
    private func parseWarpLayerResponse(response: String?) -> WarpState? {
        guard let response = response else {
            return nil
        }

        var states: [WarpState.LayerState] = []
        for character in response {
            if let intValue = Int(String(character)),
                let state = WarpState.LayerState(rawValue: intValue) {
                states.append(state)
            } else {
                NSLog("Unknown character \(character) in response: \(response)")
                return nil
            }
        }

        let warpState = WarpState()
        warpState.layerStates = states.reversed()
        warpState.isCurrentPhysicalState = true

        NotificationManager.shared.warpStateDidUpdate()

        return warpState
    }

    // For debugging state updates you can check the current state without pestering the physical loom.
    func currentImmediateWarpState() -> WarpState? {
        return  warpState
    }

    /// The current idea about which warp layers are associated with which levers on the loom.
    //  This originally comes from asking the loom.  The loom board will assume all states are unknown until
    //  an IR scan is performed.  Scanning is separate from calling this.  This will never institute  scan.
    ///
    /// - Parameters:
    ///   - forceUpdate: If true this will never return a cached version and will instead always ask the loom.
    ///   - completion: Called with the results of either the cache or the values returned by the loom.
    func getWarpState(forceUpdate: Bool, completion: @escaping (_ warpState: WarpState?, _ error: Error?) -> Void) {
        // Return the cached value if we have one.
        if let warpState = warpState,
            !forceUpdate {
            completion(warpState, nil)
            return
        }

        guard let count = loomWarpCount else {
            completion(nil, LoomError.layerCountNotYetEstablished)
            return
        }

        // Right now return values are limited to about 30 characters so we do a per layer round trip
        // request.  This could be spead up if that were fixed.  Or even if we just return 20 layer chunks.
        var newLayerStates: [WarpState.LayerState] = []

        let scanQueue = DispatchQueue(label: "warpStateRequestQueue")
        let orderingGroup = DispatchGroup()
        let requestGroup = DispatchGroup()

        var firstError: Error?

        for index in 0..<count {
            requestGroup.enter()
            scanQueue.async {
                // Everyone waits on the ordering group so we make these requests sequentially
                orderingGroup.wait()
                orderingGroup.enter()

                if firstError != nil {
                    orderingGroup.leave()
                    requestGroup.leave()
                    return
                }

                self.requestLayerState(for: index) { (state, error) in
                    if let state = state {
                        newLayerStates.append(state)
                    }
                    if error != nil {
                        firstError = error
                    }
                    orderingGroup.leave()
                    requestGroup.leave()
                }
            }
        }

        requestGroup.notify(queue: .main) {
            if firstError != nil {
                completion(nil, firstError)
                return
            }
            let warpState = WarpState()
            warpState.layerStates = newLayerStates
            warpState.isCurrentPhysicalState = true

            self.warpState = warpState
            NotificationManager.shared.warpStateDidUpdate()

            completion(warpState, nil)
        }
    }

    private func requestLayerState(for warpLayer: Int, completion: @escaping (_ state: WarpState.LayerState?, _ error: Error?) -> Void) {
        KSBluetoothManager.shared.sendCommand("gc \(warpLayer) ", annotation: "Get Warp Layer State",
                                              success: { response in
                                                if let responseString = response,
                                                    let intResponse = Int(responseString),
                                                    let state = WarpState.LayerState(rawValue: intResponse) {
                                                    completion(state, nil)
                                                } else {
                                                    completion(nil, LoomError.invalidResponse)
                                                }},
                                              failure: { response in completion(nil, self.failureError())}, finally: {})
    }
    
    /// When about to turn the loom off this warns the loom about that so it can save stuff in
    /// non volatile memory.  Right now this doesn't do much.
    func prepareForShutdown(completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        KSBluetoothManager.shared.sendCommand("x", annotation: "Prepare for Shutdown",
                                              success: { response in
                                                completion(true, nil)
        }, failure: { response in completion(false, self.failureError())}, finally: {})
    }

    /// Using the current state of the cams, this decides if a + or - lever would be more efficient in terms of
    /// total cam motion. So for example doing a plain weave should just be able to alternate between + and -
    /// levers without any cam motion. This returns nil if we do not currently know the cam state of the loom.
    /// The incoming leverType is ignored.
    func getLeverTypeForOptimizedCamMotion(to weftPick: WeftPick) -> WeftPick.LeverType? {
        guard let count = loomWarpCount,
            let warpState = warpState,
            warpState.layerStates.count == count else {
                return nil
        }

        guard let currentStateString = stringForLayerStates(warpState.layerStates) else {
            NSLog("Some cams are in unknown positions so we can not optimize.")
            return nil
        }

        let positiveWeftStateString = stringForWeftPick(weftPick, loomWarpCount: count)
        let negativeWeftStateString = invertWeftPickString(positiveWeftStateString)

        let positiveCamsChanging = zip(currentStateString, positiveWeftStateString).filter { $0 != $1 }.count
        let negativeCamsChanging = zip(currentStateString, negativeWeftStateString).filter { $0 != $1 }.count

        return positiveCamsChanging < negativeCamsChanging ? WeftPick.LeverType.positive : WeftPick.LeverType.negative
    }

    /// This sends the new cam state required for this weftPick to the loom.
    func sendWeftPickToLoom(_ weftPick: WeftPick, completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        if let count = loomWarpCount {
            sendWeftPickToLoom(weftPick, loomWarpCount: count, completion: completion)
        } else {
            getLoomWarpCount() { count, error in
                if let warpCount = count {
                    self.sendWeftPickToLoom(weftPick, loomWarpCount: warpCount, completion: completion)
                } else {
                    completion(false, error)
                }
            }
        }
    }

    private func invertWeftPickString(_ weftString: String) -> String {
        var invertedString = ""
        for character in weftString {
            if character == "0" {
                invertedString += "1"
            } else if character == "1" {
                invertedString += "0"
            } else {
                assertionFailure("Weft strings can only contain 0s and 1s")
            }
        }
        return invertedString
    }

    private func stringForLayerStates(_ warpStates: [WarpState.LayerState]) -> String? {
        var string = ""
        for state in warpStates {
            switch state {
            case .liftsWithPlusLever:
                string += "0"
            case .liftsWithMinusLever:
                string += "1"
            case .unknown:
                return nil
            }
        }
        return string
    }
    
    private func stringForWeftPick(_ weftPick: WeftPick, loomWarpCount: Int) -> String {
        // Apply from left to right so the 0's and 1's in the logs are visually in the same order.
        // 0 being warp over weft when the positive lever is pulled.
        // 1 being weft over warp when the positive lever is pulled.
        let onTopString = weftPick.leverType == .positive ? "0" : "1"
        let onBottomString = weftPick.leverType == .positive ? "1" : "0"
        var string = ""
        for x in 0..<loomWarpCount {
            // If it's not part of the pick it defaults to 0
            // This doesn't work any more since + and - lever optimization may involve cams outside the pick range.
//            if x < weftPick.minX || x > weftPick.maxX {
//                string += "0"
//                continue
//            }
            string +=  weftPick.patternLayer.isOnTop(.warp, x: x, y: weftPick.y) ? onTopString : onBottomString
        }
       
        return string
    }

//    private func updateStateWithFailedWeftPick(_ weftPick: WeftPick) {
//        let count = loomWarpCount ?? defaultWarpCount
//        var newWarpStates = Array.init(repeating: Loom.WarpLayerState.liftsWithPlusLever, count: count)
//        for x in 0..<count {
//            newWarpStates[x] =  weftPick.patternLayer.isOnTop(.warp, x: x, y: weftPick.y) ? .liftsWithPlusLever : .liftsWithMinusLever
//        }
//        warpLayerStates = newWarpStates
//        matchesPhysicalLoomState = false
//    }

    private func sendWeftPickToLoom(_ weftPick: WeftPick, loomWarpCount: Int,
                                    completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        let weftString = stringForWeftPick(weftPick, loomWarpCount: loomWarpCount)
        if weftString == "" {
            completion(false, LoomError.zeroWidthPattern)
            return
        }
        
        KSBluetoothManager.shared.sendCommand("sp " + weftString, annotation: "Send Weft Pattern",
                                              success: { response in
                                                completion(true, nil)
        }, failure: { response in
 //           self.updateStateWithFailedWeftPick(weftPick)
            completion(false, self.failureError())},
           finally: {})
    }

    // Currently the failure state has no useful error payload, so we generate this default
    // response that at least tells you if anything was connected or not.
    private func failureError() -> Error {
        if KSBluetoothManager.shared.connectionState != .connected {
            return LoomError.notConnected
        }
        return LoomError.unexplainedFailure
    }

}

extension Loom: KSBluetoothManagerStateListener {

    // If we get disconnected we have to forget about all the cached state inforation,
    // and request it again the next time we need it.
    func connectionStateChanged(_ connectionState: KSPeripheralConnectionState) {
        if connectionState != .connected {
            loomWarpCount = nil
            warpState = nil
        }
    }

}

extension Loom: KSBluetoothManagerCommandListener {

    func didReceiveCommand(_ command: String, argument: String) {
        // This is called after a scan, and also after sending of a row of cam positions finished.
        // We respond by updating our ideas about all the cams.
        if command == "nc" {
            Loom.shared.getWarpState(forceUpdate: true) { (warpState, error) in
                if let warpState = warpState {
                    NSLog("Got layer states \(warpState.layerStates)")
                } else {
                    NSLog("Error getting layer states \(error?.localizedDescription ?? "nil")")
                }
            }
        }
    }

}
