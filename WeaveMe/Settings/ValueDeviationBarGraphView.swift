//
//  ValueDeviationBarGraphView.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 4/29/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//

import UIKit

class ValueDeviationBarGraphView: UIView {

    var values: [Double] = []

    var aboveAverageBarColor = UIColor.lightGray
    var belowAverageBarColor = UIColor.black
    var centerLineColor = UIColor.gray
    var errorColor = UIColor.red.withAlphaComponent(0.4)
    var spacing: CGFloat = 2

    func setValuesFromPositions(_ positions: [Int]) {
        if positions.count < 2 {
            values = []
            return
        }

        var newValues: [Double] = []
        newValues.reserveCapacity(positions.count - 1)

        var maxStep = 0.0
        var stepsTotaled = 0.0
        for (index, position) in positions.enumerated() {
            if index == 0 {
                continue
            }

            let step = Double(position - positions[index - 1])
            stepsTotaled += step
            if step > maxStep {
                maxStep = step
            }
            newValues.append(step)
        }
        let averageStep = stepsTotaled/Double(newValues.count)

        for (index, value) in newValues.enumerated() {
            newValues[index] = value - averageStep
        }

        values = newValues
    }

    private func averageValue() -> Double? {
        if values.count < 1 {
            return nil
        }
        var total = 0.0
        for value in values {
            total += value
        }
        return total/Double(values.count)
    }

    private func drawDashedLine(in context: CGContext, from point0: CGPoint, to point1: CGPoint) {
        context.saveGState()

        context.move(to: point0)
        context.addLine(to: point1)

        let  dashes: [CGFloat] = [10, 5]
        context.setLineDash(phase: 0, lengths: dashes)
        context.setLineWidth(1.0)
        context.setStrokeColor(centerLineColor.cgColor)

        context.strokePath()
        context.restoreGState()
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext(),
            !values.isEmpty else {
                return
        }

        let centerY = bounds.size.height/2

        var x: CGFloat = spacing

        // We also put spacing on the left/right of the bars.
        let barWidth = (bounds.size.width - spacing*CGFloat(values.count + 1))/CGFloat(values.count)

        // Draw bars from left to right extending either up or down from the horizontal center line.
        for value in values {
            if value > 0 {
                context.setFillColor( centerY - CGFloat(value) < 0 ? errorColor.cgColor : aboveAverageBarColor.cgColor )
//                context.setFillColor(aboveAverageBarColor.cgColor)
                context.addRect(CGRect(x: x, y: centerY, width: barWidth, height: CGFloat(value)))
                context.fillPath()
            } else {
                context.setFillColor(centerY + CGFloat(value) > bounds.size.height ? errorColor.cgColor : belowAverageBarColor.cgColor)
                context.addRect(CGRect(x: x, y: centerY + CGFloat(value), width: barWidth, height: CGFloat(-value)))
                context.fillPath()
            }
            x += spacing + barWidth
        }

        // Draw horizontal dashed line down the middle.
        //drawDashedLine(in: context, from: CGPoint(x: 0, y: centerY), to: CGPoint(x: bounds.size.width, y: centerY))
    }
}
