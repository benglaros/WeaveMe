//
//  JogHandler.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 5/11/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  There is some state associated with sending jog commands to the bluetooth device.
//  Since we want to use that for various different kinds of jog commands we package that up
//  in this helper, and it does all the work of tracking the state of the communication.
//

import UIKit

class JogHandler: NSObject {
    var command: String
    var commandAnnotation: String
    weak var jogSlider: UISlider?

    weak var alphaLimitTracker: LimitIndicatorTracker?
    weak var betaLimitTracker: LimitIndicatorTracker?

    init(command: String, commandAnnotation: String, slider: UISlider) {
        self.command = command
        self.commandAnnotation = commandAnnotation
        jogSlider = slider
    }
    
    func sliderValueChanged(slider: UISlider) {
        assert(slider == jogSlider, "JogHandler: slider value changes reported to wrong jogHandler.")
        
        if !isDraggingSlider {
            dragBegan(slider: slider)
        }
        
        sendJogValue(Int(254*slider.value + 1))
    }
    
    private(set) var isDraggingSlider: Bool = false
    private var canSendJogValues: Bool = false
    private var unsentJogValue: Int?
    
    // We send raw numbers to the jog loop.  128 is not jogging, 1 -> 255 for full jog range,
    // but we send 0 when we're done.
    let jogFinishedCode: Int = 0
    
    private func dragBegan(slider: UISlider) {
        NSLog("BEGIN")
        isDraggingSlider = true
        canSendJogValues = false

        beginIndicatorTracking()

        KSBluetoothManager.shared.sendCommand(command, annotation: commandAnnotation, success: { response in
            // If the drag ended before we even got the board into jog mode, we have to tell it to
            // get out of jog mode and finish.
            if !self.isDraggingSlider {
                self.sendJogValue(self.jogFinishedCode)
                return
            }
            
            self.canSendJogValues = true
            
            // If we'd gotten any slider values while waiting to get in the jog mode we send those out now.
            if let value = self.unsentJogValue {
                self.sendJogValue(value)
            }
        }, failure: { response in
            NSLog("JogHandler: send command failed for %@", self.command)
        }, finally: {})
    }
    
    func dragEnded(slider: UISlider) {
        sendJogValue(jogFinishedCode)
        isDraggingSlider = false
        canSendJogValues = false
        
        NSLog("END")
        endIndicatorTracking()
       
        // Weirdly if this animation block isn't used the animation doesn't happen.
        // Possibly use spring animation.
        UIView.animate(withDuration: 0.15) {
            slider.setValue(0.5, animated: true)
        }
    }
    
    private func sendJogValue(_ value: Int) {
        assert(value >= 0 && value <= 255)
        
        if !canSendJogValues {
            unsentJogValue = value
            return
        }
        
        unsentJogValue = nil
        
        var uCharValue: UInt8 = UInt8(value)
        let data = Data.init(bytes: &uCharValue, count: 1)
        KSBluetoothManager.shared.writeRawData(data)
    }

    private func beginIndicatorTracking() {
        alphaLimitTracker?.isActive =  true
        betaLimitTracker?.isActive = true
    }

    private func endIndicatorTracking() {
        alphaLimitTracker?.isActive =  false
        betaLimitTracker?.isActive = false
    }
}
