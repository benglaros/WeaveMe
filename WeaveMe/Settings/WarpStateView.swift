//
//  WarpStateView.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 5/11/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//

import UIKit

class WarpStateView: UIView {

    var warpState: WarpState? {
        didSet {
            setNeedsDisplay()
        }
    }

    private let physicalStateColor = UIColor.gray
    private let virtualStateColor = UIColor.lightGray

    private let lineWidth: CGFloat = 1.0

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext(),
            let warpState = warpState,
            warpState.layerStates.count != 0 else {
            return
        }
        
        context.setStrokeColor(strokeColorForWarpState(warpState).cgColor)
        context.setFillColor(fillColorForWarpState(warpState).cgColor)
        context.setLineWidth(lineWidth)

        let width = bounds.size.width
        let height = bounds.size.height
        let count = CGFloat(warpState.layerStates.count)

        let inset: CGFloat = 2
        let backMargin: CGFloat = height*0.3
        let forwardY: CGFloat = height
        let forwardInsetY: CGFloat = forwardY - inset
        let backY = 0 + backMargin
        let backInsetY = backY + inset
        let fullBackY: CGFloat = 0

        let xStep = -(width - lineWidth)/count

        var x: CGFloat = width - lineWidth/2
        var y: CGFloat = fullBackY
        var insetY: CGFloat = fullBackY

        context.move(to: CGPoint(x: x, y: y))
        for state in warpState.layerStates {
            if state == .liftsWithPlusLever {
                y = forwardY
                insetY = forwardInsetY
            } else if state == .liftsWithMinusLever {
                y = backY
                insetY = backInsetY
            } else {
                y = fullBackY
                insetY = fullBackY
            }
            context.addLine(to: CGPoint(x: x, y: y))
            x += xStep
            context.addLine(to: CGPoint(x: x, y: y))
            context.addLine(to: CGPoint(x: x, y: insetY))
        }
        context.addLine(to: CGPoint(x: x, y: fullBackY))
        context.closePath()
        context.drawPath(using: .fillStroke)
    }

    private func strokeColorForWarpState(_ warpState: WarpState) -> UIColor {
        return warpState.isCurrentPhysicalState ? .black : virtualStateColor
    }

    private func fillColorForWarpState(_ warpState: WarpState) -> UIColor {
        return warpState.isCurrentPhysicalState ? physicalStateColor : virtualStateColor
    }

}
