//
//  DataGraphHandler.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 5/23/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This packages up a command that requests data from the blue tooth device.
//  Mostly this is to display streams of 0->1024 Analog read values, but it could
//  be used to request values from some other source.
//
//  It pushes the data into a lineGraphView.
//
//  Currently every time the update is called it will request more data and append it to
//  the existing data so the graph will progress with the data moving right to left in
//  the graph view.
//
//  TODO: Right now this is used for slow polled data, but if the device is collecting the data
//  at a faster rate and sending us it in chunks this could eventually be extended to have a
//  full update mode.
//
//  We may also want to write a real time stamped version of these values so the graph can be
// against "time" in a more real way even if the polling is erratic.
//

import UIKit

class DataGraphHandler: NSObject {
    var command: String!
    var commandAnnotation: String!
    weak var lineGraphView: LineGraphView!
    
    // We default to the range an 10 bit ADC would return but this could be anything.
    var valueRangeMin: CGFloat = 0
    var valueRangeMax: CGFloat = 1023
    
    // This is polling at close to the speed slightly over the speed of the command/response loop.
    var pollingInterval: TimeInterval = 0.05
    var pollingEnabled = false {
        didSet {
            if pollingEnabled {
                scheduleTimer()
            } else {
                cancelTimer()
            }
        }
    }

    private var timer: Timer?
    private func scheduleTimer() {
        if timer != nil {
            return
        }
        timer = Timer.scheduledTimer(withTimeInterval: pollingInterval, repeats: true) { [weak self] timer in
            if self != nil && !self!.requestInProgress {
                self?.update()
            } else {
                NSLog(".")
            }
        }
    }
    
    private func cancelTimer() {
        guard let timer = timer else {
            return
        }
        timer.invalidate()
        self.timer = nil
    }
    
    init(command: String, commandAnnotation: String, lineGraphView: LineGraphView) {
        self.command = command
        self.commandAnnotation = commandAnnotation
        self.lineGraphView = lineGraphView
    }
    
    private var requestInProgress = false
    
    // Called at your own pace, this will request some data and append it to the
    // right hand side of the graph view.
    func update() {
        lineGraphView.valueRangeMin = valueRangeMin
        lineGraphView.valueRangeMax = valueRangeMax
        
        requestInProgress = true
        KSBluetoothManager.shared.sendCommand(command, annotation: commandAnnotation, success: { response in
            // Right now we just use a lame string encoding of the number.   That's nice and simple
            // from a debugging standpoint, but eventually for faster updates we could support some
            // faster raw data transmission.
            
            guard let response = response else {
                NSLog("No response give on success for command \(self.command ?? "nil") \(self.commandAnnotation ?? "nil")")
                return
            }
            
            var valueString =  response.replacingOccurrences(of: "*", with: "")
            valueString = valueString.trimmingCharacters(in: .whitespaces)
            
            guard let value = Int(valueString) else {
                NSLog("DataGraphHandler: Unable to convert value \"\(valueString)\" to an integer" )
                return
            }
            
            self.lineGraphView.values.append(CGFloat(value))
            
            // If there are now more values than will fit in the visible range we trim off the left most values.
            let visibleValues = Int(floor(self.lineGraphView.bounds.size.width/self.lineGraphView.xStep))
            if self.lineGraphView.values.count > visibleValues {
                self.lineGraphView.values.removeSubrange(0..<self.lineGraphView.values.count - visibleValues)
            }
            
            self.lineGraphView.setNeedsDisplay()
            
        }, failure: { response in
            NSLog("DataGraphHandler: send command failed for \(self.command ?? "nil")")
        }, finally: {
            self.requestInProgress = false
        })
    }
}
