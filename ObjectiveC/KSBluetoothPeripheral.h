//
//  KSBluetoothPeripheral.h
//
//  Created by Kurt Schaefer on 5/9/15.
//  Copyright (c) 2015 Kurt Schaefer. All rights reserved.
//
//  This is a front end for bluetooth LE communications. It can scan for and connect to a
//  bluetooth peripheral. Using the UARTPeripheral directly sort of works, but I've had a
//  hard time with random silent failures.  Possibly associated with sending too much
//  info at one time and swamping the 9600 baud connection.
//
//  This communication requires that the Arduino confirm receipt of each string, and also allows a
//  for per line responses from the Arduino.  That seems to keep the sending buffer from
//  overflowing and lets us make specific requests of the Arduino with an easy async bock based interface.
//
//  Responses must be of the form *<response> or just * for a positive ack without response.
//  Data sent from the Arduino without the * prefix are assumed to not be associated with a sent string
//  and are instead sent to the didRecieveString method on the delegate.
//
//  This lib doesn't try to do anything fancy with retrying/checksum's etc.
//  The hope is that this is enough to mostly work without having to get too fancy, and if we do have
//  to improve it later the interface can remain the same.
//
//  We migth eventually want to expand the interface for sending block data.

#import <Foundation/Foundation.h>
#import "UARTPeripheral.h"


typedef NS_ENUM(NSInteger, KSPeripheralConnectionState) {
    KSPeripheralConnectionStateNotConnected = 0,
    KSPeripheralConnectionStateScanning,
    KSPeripheralConnectionStateConnected,
};


@protocol KSBluetoothPeripheralDelegate <NSObject>
- (void)didReceiveString:(NSString*)string;
- (void)connectionStateChanged:(KSPeripheralConnectionState)connectionState;
@end

@interface KSBluetoothPeripheral : NSObject
@property (nonatomic, assign) KSPeripheralConnectionState connectionState;
@property (nonatomic, weak) id<KSBluetoothPeripheralDelegate> delegate;

- (void)scanForPeripherals;
- (void)disconnect;

- (void)sendString:(NSString*)string
           success:(void (^)(NSString*response))success
           failure:(void (^)(NSString*error))failure
           finally:(void (^)(void))finally;

- (void)sendStringArray:(NSArray*)stringArray
                success:(void (^)(NSString*response))success
                failure:(void (^)(NSString*error))failure
                finally:(void (^)(void))finally;

// This is for when you want low overhead communication for special transfers.
// This is for example used by the jog controller to send individual bytes
// where we don't want to wait for a response, etc.
// It's up to the caller to not flood this and overflow things.
- (void)writeRawData:(NSData*)data;

@end
