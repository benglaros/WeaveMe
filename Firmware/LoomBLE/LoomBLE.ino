#include <AccelStepper.h>
#include <SPI.h>
#include "Adafruit_BLE_UART.h"
#include <Adafruit_NeoPixel.h> // https://github.com/adafruit/Adafruit_NeoPixel
#include <EEPROM.h>

// To build this you'll need https://github.com/adafruit/Adafruit_nRF8001 for the bluetooth
// and http://www.airspayce.com/mikem/arduino/AccelStepper/ for the stepper code.

// Connect CLK/MISO/MOSI to hardware SPI
// e.g. On UNO & compatible: CLK = 13, MISO = 12, MOSI = 11
// For the Atmega644P Use the MightyCore Board: Atmega644, Variant: 644P/44PA,  Pinout: Bobuino
// If the Adafruit_BLE_UART doesn't support 644P yet, try adding:
//
// #elif  defined(__AVR_ATmega644P__) || defined(__AVR_ATmega644__)
//  6,  2,
//  2,  0,
//  3,  1,
//
// to the end of  dreqinttable in hal_aci_tl.cpp
//

#define BLE_SPI_CLK  13
#define BLE_SPI_MISO 12
#define BLE_SPI_MOSI 11
#define BLE_REQ 20
#define BLE_RST 17

// This is rather specialized because I wired READY to PA2 26 on the board, but that isn't an external
// Interupt, so I have a cut/oops wire going over to the middle of the aux pins, which is wired to INT2.

#define BLE_RDY 6     // This should be an interrupt pin, on Uno thats #2 or #3

// Motor Pins
//    MS1  MS2  MS3
//     0    0    0  Full Step
//     1    0    0  Half Step
//     0    1    0  Quarter Step
//     1    1    0  Eighth Step
//     0    0    1  Sixteenth Step
//     1    1    1  Thirtysecondth Step

#define MOTORS_MS1_DIG_OUT  28
#define MOTORS_MS2_DIG_OUT  27
#define MOTORS_MS3_DIG_OUT  26
#define MOTORS_NOT_RESET_DIG_OUT  25
#define MOTORS_NOT_SLEEP_DIG_OUT  24

// This is defined by the motor type.  This is before micro steps are applied.
#define MOTOR_NATIVE_STEPS_PER_REVOLUTION 200

// These used to be dynamically set-able but now we're locking down on 
// a specific number of cams/micro steps, etc.  That makes it easier to deal with
// hard coded cam positions.
#define MOTOR_MICROSTEPS_PER_STEP    4
#define MOTOR_STEPS_PER_REVOLUTION   MOTOR_NATIVE_STEPS_PER_REVOLUTION*MOTOR_MICROSTEPS_PER_STEP

// These have to be determined based on load/what AccelStepper can handle at chip clock speed.
#define MOTOR_MAX_SPEED 2000
#define MOTOR_MAX_ACCELERATION 3000

// Labled Motor 1 on the PCB
// This motor turns the lead screw that moves the carriage.
#define CARRIAGE_MOTOR_STEP_DIG_OUT        23
#define CARRIAGE_MOTOR_DIR_DIG_OUT         22
#define CARRIAGE_MOTOR_NOT_ENABLED_DIG_OUT 29

// Labled Motor 2 on the PCB
// This motor is located to the weavers right when sitting in front of the machine
// It uses a square drive section to rotate cams.  One for each warp thread.
#define RIGHT_CAM_MOTOR_STEP_DIG_OUT        9
#define RIGHT_CAM_MOTOR_DIR_DIG_OUT         8
#define RIGHT_CAM_MOTOR_NOT_ENABLED_DIG_OUT 31

// Labled Motor 3 on the PCB
// This motor is located to the weavers left when sitting in front of the machine.
#define LEFT_CAM_MOTOR_STEP_DIG_OUT        3
#define LEFT_CAM_MOTOR_DIR_DIG_OUT         2
#define LEFT_CAM_MOTOR_NOT_ENABLED_DIG_OUT 30

// We can control a string of neo pixels though this digital out, currently we only user
// one for showing Bluetooth state, but we could hook up more in order to indicate
// which lever to flip warp thread color, or other things.
#define LED_DATA_DIG_OUT                   15
#define LED_COUNT                          2

// The V1->V3 bordes have these swapped, but with the later revs of the
// case the "r_ir" plug interfears with the arrow button, so I'm swapping them.
// The V4 version will have the labels swapped as well to match this.
#define RIGHT_IR_SENSOR_ANALOG_IN          A7
#define LEFT_IR_SENSOR_ANALOG_IN           A2
#define SENSOR_SAMPLE_SIZE 4
// This value should mosty work for blocked/not blocked.  You can use the app's IR sensor scan
// to get a histogram of values in various situations if you want to adjust this value.
#define IR_SENSOR_THRESHOLD   850

// This is named for the position of the switch, not the direction of travel
// of the carriage when the switch would activate.
#define LIMIT_SWITCH_RIGHT_DIG_IN          7
#define LIMIT_SWITCH_LEFT_DIG_IN           10
#define DEBOUNCE_LIMIT 3

// These are the on PCB switches.
#define NEXT_SWITCH_DIG_IN                  5
#define PREV_SWITCH_DIG_IN                  4

#define SENSOR_LEFT  0
#define SENSOR_RIGHT 1

// Temp list of connection status we display on the status LED.
#define STATUS_OFF           0
#define STATUS_CONNECTED     1
#define STATUS_NOT_CONNECTED 2
#define STATUS_ERROR         3

// During interactive jogs we use the thread led to display jog status so we can
// flag limit hits visually.
#define JOG_STATUS_INACTIVE  0 
#define JOG_STATUS_ACTIVE    1
#define JOG_STATUS_WARN      2  // Eventually for as yet unimplemented soft limit.
#define JOG_STATUS_LIMIT_HIT 3
#define JOG_STATUS_UNKNOWN   4  // Used so we can avoid unneeded led.show() calls and for sharing the AUX_LED.

// Colors we may use in the display, right now only a few like gold/red/green are used.
#define COLOR_GOLD 0xfe7000
#define COLOR_COPPER 0xff3000
#define COLOR_PINK 0xfe0032
#define COLOR_GREEN 0x00ff00
#define COLOR_BLUE  0x0000ff
#define COLOR_RED 0xff0000
#define COLOR_YELLOW 0xfea100
#define COLOR_TURQUOISE 0x00ffff
#define COLOR_ORANGE 0xfe1a00
#define COLOR_PURPLE 0xff00ff
#define COLOR_MOON_BRIGHT 0xfff8d1
#define COLOR_MOON_DIM 0x0f120d
#define COLOR_EYE_GREEN 0x00c800
#define COLOR_WHITE 0xffffff
#define COLOR_ALMOST_WHITE 0xfffffe

#define SEND_FORCED   true
#define SEND_OPTIONAL false

#define CARRIAGE_PITCH_IN_MM    8.0

// Sent when an interactive jog finishes.
#define JOG_FINISHED_CODE 0

// The stepper motor controll objects.
AccelStepper gCarriageStepper(AccelStepper::DRIVER, CARRIAGE_MOTOR_STEP_DIG_OUT, CARRIAGE_MOTOR_DIR_DIG_OUT);
AccelStepper gRightCamStepper(AccelStepper::DRIVER, RIGHT_CAM_MOTOR_STEP_DIG_OUT, RIGHT_CAM_MOTOR_DIR_DIG_OUT);
AccelStepper gLeftCamStepper(AccelStepper::DRIVER, LEFT_CAM_MOTOR_STEP_DIG_OUT, LEFT_CAM_MOTOR_DIR_DIG_OUT);

// The Blue Tooth serial object and status.
Adafruit_BLE_UART BTLESerial = Adafruit_BLE_UART(BLE_REQ, BLE_RDY, BLE_RST);
aci_evt_opcode_t gLastStatus = ACI_EVT_INVALID;
aci_evt_opcode_t gStatus = ACI_EVT_INVALID;

// If the board isn't installed, or the chip has blown it will never start so we keep track of that
// so we don't give the impression of seaking a BT connection when there is no chance of one.
bool gBTDeviceEverStarted = false;


// The leds we use to communicate state to the user.
Adafruit_NeoPixel LEDs = Adafruit_NeoPixel(LED_COUNT, LED_DATA_DIG_OUT, NEO_GRB + NEO_KHZ800);
#define STATUS_LED 0
#define AUX_LED 1


// These are all set once a microstepping amount is chosen/user sets home/end/number of warp layers.

// When cams are rotated we always overshoot and return a little to clear any pressure
// on the cam.  This is the fraction of a rotation that we overshoot.
#define CAM_OVERSHOOT_ROTATION_DIVISOR 128

// Currently we limit this so we can statically allocate this amount of buffer up front.
#define NUMBER_OF_WARP_LAYERS 60

// Right now we use a full byte per state.
unsigned char gCurrentCamStates[NUMBER_OF_WARP_LAYERS];
unsigned char gTargetCamStates[NUMBER_OF_WARP_LAYERS];

// Cam associates that layer with the + lever, which is user forward.
#define CAM_STATE_POSITIVE_LEVER 0
// Cam associates that layer with the - lever, which is user back.
#define CAM_STATE_NEGITIVE_LEVER 1
#define CAM_STATE_UNKNOWN        2

// Because just using the average cam width from home/end does not deal with batches of cams 
// that are slighty narrower/wider, I actually measured the cam positions for every 10th cam.
// This locks us in to 60 cams and these values will have to be updated when ever the loom is rebuilt, but
// that seems like a reasonable thing to do if it's the difference between the loom working and just 
// jamming up at the higher cam numbers.  
// -1 is impossible to mesure but you can compute it by measuring cam 0 and then back projecting one step from there.
// Cam indexs mesured:         -1    9     19    29     39    49    59
long gMeasuredPositions[7]  = { 322, 3393, 6418, 9432, 12452, 15476, 18548 };

#define EEPROM_INFO_ADDRESS 0
#define EEPROM_NEVER_WAS_SET 255
#define EEPROM_INFO_VERSION 2   // Increment this if you change/reorder the EEPROMInfo
#define UNDEFINED_LIMIT_OFFSET -99999999

typedef struct {
    unsigned char version;  // incremented if the info struct changes, also used to check
                            // never having been written state, if this changes it just loads defaults.

    // It's best practice to always home the loom manually in case the cam stack has shifted.  However
    // when one does that the loom could record that position's distance from the limit switch and 
    // make it so users can auto home the carraige if they're resonably certain nothing has moved.
    long  offsetFromLimitSwitchToHome;   

} EEPROMInfo;

EEPROMInfo gInfo;

void readAndValidateEEPromInfo()
{
    EEPROM.get(EEPROM_INFO_ADDRESS, gInfo);

    // If we have never saved this info off to eeprom or the version has changed
    // we mark the limit switch distance as unknown
    if (gInfo.version != EEPROM_INFO_VERSION) {
        gInfo.offsetFromLimitSwitchToHome = UNDEFINED_LIMIT_OFFSET;      
        writeEEProm();
    }
}

void writeEEProm()
{
    // Always stamp the written info with our current version.
    gInfo.version = EEPROM_INFO_VERSION;
    EEPROM.put(EEPROM_INFO_ADDRESS, gInfo);
}

void setup(void)
{ 
    Serial.begin(9600);
    Serial.println(F("LoomBLE V0.3"));
    
    pinMode(MOTORS_MS1_DIG_OUT, OUTPUT);
    pinMode(MOTORS_MS2_DIG_OUT, OUTPUT);
    pinMode(MOTORS_MS3_DIG_OUT, OUTPUT);
    pinMode(MOTORS_NOT_RESET_DIG_OUT, OUTPUT);
    pinMode(MOTORS_NOT_SLEEP_DIG_OUT, OUTPUT);

    pinMode(CARRIAGE_MOTOR_STEP_DIG_OUT, OUTPUT);
    pinMode(CARRIAGE_MOTOR_DIR_DIG_OUT, OUTPUT);
    pinMode(CARRIAGE_MOTOR_NOT_ENABLED_DIG_OUT, OUTPUT);

    pinMode(RIGHT_CAM_MOTOR_STEP_DIG_OUT, OUTPUT);
    pinMode(RIGHT_CAM_MOTOR_DIR_DIG_OUT, OUTPUT);
    pinMode(RIGHT_CAM_MOTOR_NOT_ENABLED_DIG_OUT, OUTPUT);

//    pinMode(LEFT_CAM_MOTOR_STEP_DIG_OUT, OUTPUT);
//    pinMode(LEFT_CAM_MOTOR_DIR_DIG_OUT, OUTPUT);
//    pinMode(LEFT_CAM_MOTOR_NOT_ENABLED_DIG_OUT, OUTPUT);

    pinMode(RIGHT_IR_SENSOR_ANALOG_IN, INPUT);
    pinMode(LEFT_IR_SENSOR_ANALOG_IN, INPUT);

    pinMode(LIMIT_SWITCH_RIGHT_DIG_IN, INPUT_PULLUP);    
    pinMode(LIMIT_SWITCH_LEFT_DIG_IN, INPUT_PULLUP);

    pinMode(NEXT_SWITCH_DIG_IN, INPUT_PULLUP);
    pinMode(PREV_SWITCH_DIG_IN, INPUT_PULLUP);

    // Read this in very early in case we want to eventually have
    // tuned accelerations stored in the gInfo.
    readAndValidateEEPromInfo();
    
    // We should eventually tune these values
    // so we can get the best speed out of the rig.
    gCarriageStepper.setPinsInverted(true);
    gCarriageStepper.setMaxSpeed(MOTOR_MAX_SPEED);
    gCarriageStepper.setAcceleration(MOTOR_MAX_ACCELERATION);
    gCarriageStepper.setCurrentPosition(0);

    gRightCamStepper.setPinsInverted(true);
    gRightCamStepper.setMaxSpeed(MOTOR_MAX_SPEED);
    gRightCamStepper.setAcceleration(MOTOR_MAX_ACCELERATION);
    gRightCamStepper.setCurrentPosition(0);

    // Express the microsteps that were saved in the info/or the default.  This is usually not
    // something one tweaks, but for now we do so we can tune what works.
    updateStepsPerRevolutionBasedOnMicrosteps(MOTOR_MICROSTEPS_PER_STEP);

    // Pull ~RESET and ~SLEEP high so the motors can wakeup and go.
    digitalWrite(MOTORS_NOT_RESET_DIG_OUT, HIGH);
    digitalWrite(MOTORS_NOT_SLEEP_DIG_OUT, HIGH);

    // Enable the carriage motor.
    digitalWrite(CARRIAGE_MOTOR_NOT_ENABLED_DIG_OUT, LOW);

    // Enable the right cam motor
    digitalWrite(RIGHT_CAM_MOTOR_NOT_ENABLED_DIG_OUT, LOW);    

    // Start with all leds off. 
    LEDs.begin();
    LEDs.setPixelColor(AUX_LED, 0);
    LEDs.setPixelColor(STATUS_LED, 0);
    LEDs.show();

    Serial.println(F("Setting Device name to Loom"));
    BTLESerial.setDeviceName("Loom");   // This can only be 7 chars long.
    BTLESerial.begin();

    initializeCamState();
}

void initializeCamState()
{
    // We initialize to all unknown until the user has performed a scan.
    for (int i=0; i<NUMBER_OF_WARP_LAYERS; ++i) {
        gCurrentCamStates[i] = CAM_STATE_UNKNOWN;
        gTargetCamStates[i] = CAM_STATE_UNKNOWN;
    }    
}

void notifyAboutNewCamPositionsAvailable() 
{
    sendStringBT(F("nc"));
}

void scanAllCamStates()
{
    LEDs.setPixelColor(AUX_LED, scaleColor(COLOR_BLUE, 100));
    // Scan from left to right. Scanning toward home will be smoother since that's the
    // cannonical anti backlash direction.  Show the state in the aux led for quick visual testing.
    for (int i=NUMBER_OF_WARP_LAYERS - 1; i >= 0; --i) {
        seekToWarpLayer(i);
        // We may want to use two threshold and have an Unknown state for uncertain cam positions,
        // but for now we make due with black and white decisions.
        if (irSensorIsBlocked(SENSOR_RIGHT)) {    // Cam forward blocks the sensor.
            gCurrentCamStates[i] = CAM_STATE_POSITIVE_LEVER;
            LEDs.setPixelColor(AUX_LED, scaleColor(COLOR_BLUE, 200));
        } else {
            gCurrentCamStates[i] = CAM_STATE_NEGITIVE_LEVER;
            LEDs.setPixelColor(AUX_LED, scaleColor(COLOR_TURQUOISE, 200));
        }
        LEDs.show();
    }
    LEDs.setPixelColor(AUX_LED, 0);
    LEDs.show();
    notifyAboutNewCamPositionsAvailable();
}

void updateStepsPerRevolutionBasedOnMicrosteps(int microstepsPerStep)
{
// Motor Pins
//    MS1  MS2  MS3
//     0    0    0  Full Step
//     1    0    0  Half Step
//     0    1    0  Quarter Step
//     1    1    0  Eighth Step
//     0    0    1  Sixteenth Step
//     1    1    1  Thirtysecondth Step
    
    switch(microstepsPerStep) {
    case 1:
        digitalWrite(MOTORS_MS1_DIG_OUT, LOW);
        digitalWrite(MOTORS_MS2_DIG_OUT, LOW);
        digitalWrite(MOTORS_MS3_DIG_OUT, LOW);
        break;
    case 2:
        digitalWrite(MOTORS_MS1_DIG_OUT, HIGH);
        digitalWrite(MOTORS_MS2_DIG_OUT, LOW);
        digitalWrite(MOTORS_MS3_DIG_OUT, LOW);   
        break;
    default:
        Serial.print(F("Unsupported microsteps value "));
        Serial.println(String(microstepsPerStep).c_str());
        microstepsPerStep = 4;
    case 4:
        digitalWrite(MOTORS_MS1_DIG_OUT, LOW);
        digitalWrite(MOTORS_MS2_DIG_OUT, HIGH);
        digitalWrite(MOTORS_MS3_DIG_OUT, LOW);       
        break;
    case 8:
        digitalWrite(MOTORS_MS1_DIG_OUT, HIGH);
        digitalWrite(MOTORS_MS2_DIG_OUT, HIGH);  
        digitalWrite(MOTORS_MS3_DIG_OUT, LOW);       
        break;
    case 16:
        digitalWrite(MOTORS_MS1_DIG_OUT, LOW);
        digitalWrite(MOTORS_MS2_DIG_OUT, LOW);
        digitalWrite(MOTORS_MS3_DIG_OUT, HIGH);   
        break;
    case 32:
        digitalWrite(MOTORS_MS1_DIG_OUT, HIGH);
        digitalWrite(MOTORS_MS2_DIG_OUT, HIGH);
        digitalWrite(MOTORS_MS3_DIG_OUT, HIGH); 
        break;
    }
    long stepsPerRevolution = MOTOR_NATIVE_STEPS_PER_REVOLUTION*microstepsPerStep;
    Serial.print(F("Setting steps per revolution to "));
    Serial.println(String(stepsPerRevolution).c_str());
}

void acknowlageBLE(void)
{  
    sendResponseStringBT(String(""));
}

uint32_t scaleColor(uint32_t color, uint8_t scale)
{
    // The cast chops off the high bits without having to mask them.
    uint8_t r = (uint8_t)(color >> 16);
    uint8_t g = (uint8_t)(color >> 8);
    uint8_t b = (uint8_t)(color);
   
    r = (r*scale) >> 8;
    g = (g*scale) >> 8;
    b = (b*scale) >> 8;

    return ((uint32_t )r << 16) | ((uint32_t)g << 8) | (uint32_t)b;
}

void swapFloats(float *x1, float *x2)
{
    float temp = *x1;
    *x1 = *x2;
    *x2 = temp;
}

float clamp(float x, float min, float max)
{
    if (min > max) {
        swapFloats(&min, &max);
    }

    if (x > max) {
        return max;
    } else if (x < min) {
        return min;
    } else {
        return x;
    }
}

float linear(float edge0, float edge1, float x)
{
    float t = (x - edge0)/(edge1 - edge0);
    t = clamp(t, 0.0, 1.0);

    return t;
}

float easeInOut(float edge0, float edge1, float x)
{
    float t = linear(edge0, edge1, x);

    return t*t*(3.0 - 2.0*t);
}

float easeIn(float edge0, float edge1, float x)
{
    return clamp(easeInOut(edge0, edge1 + (edge1 - edge0), x)*2.0, 0.0, 1.0);
}

float easeOut(float edge0, float edge1, float x)
{
    return clamp(easeInOut(edge0 - (edge1 - edge0), edge1, x)*2.0 - 1.0, 0.0, 1.0);
}

void setStatusLED(char status)
{
    if (status == STATUS_OFF) {
        LEDs.setPixelColor(STATUS_LED, 0);
    } else if (status == STATUS_CONNECTED) {
        // This is kind of too bright so we scale it down some.
        LEDs.setPixelColor(STATUS_LED, scaleColor(COLOR_GREEN, 200));
    } else if (status == STATUS_NOT_CONNECTED) {
        if (!gBTDeviceEverStarted) {
            // If the BT device never came on line we just show dim purple.
            LEDs.setPixelColor(STATUS_LED, scaleColor(COLOR_PURPLE, 0.5));
        } else {
            // Do breath animation at 12 cycles per minute with easeOut action both in and out.
            // That should be a natutal rate and feel nice.
            long curMillis = millis();
            long millisStep = 300;
            float value = ((float)(curMillis%5000)/2500.0);
            value = (value < 1.0) ? easeOut(0.0, 1.0, value) : easeOut(1.0, 0.0, value - 1.0);

            // We go between 210 -> 20 so never fully off or on.
            uint8_t scale = 210 - (int)(190.0*value);
            LEDs.setPixelColor(STATUS_LED, scaleColor(COLOR_GOLD, scale));
        }
            
    } else if (status == STATUS_ERROR) {
        LEDs.setPixelColor(STATUS_LED, COLOR_RED);
    }
    // For now we do a show here. But if there are going to be a lot of leds we may want to
    // make this the callers resposibilty to avoid duplicate shows().
    LEDs.show();
}

void setJogStatusLED(char status)
{
    static char mostRecentlyShownJogStatus = JOG_STATUS_UNKNOWN;


    // Avoid calling show all the time when we're doing fast stepper jogging.
    if (status == mostRecentlyShownJogStatus) {
        return;
    }

//    Serial.print(F("Jog Status: "));
//    Serial.println((int)status);
    
    if (status == JOG_STATUS_ACTIVE) {        
        LEDs.setPixelColor(AUX_LED, scaleColor(COLOR_GREEN, 200));
    } else if (status == JOG_STATUS_WARN) {
        LEDs.setPixelColor(AUX_LED, scaleColor(COLOR_GOLD, 200));
    } else if (status == JOG_STATUS_LIMIT_HIT) {
        LEDs.setPixelColor(AUX_LED, COLOR_RED);
    } else if (status == JOG_STATUS_INACTIVE) {
        LEDs.setPixelColor(AUX_LED, 0);
    }
    // For now we do a show here. But if there are going to be a lot of leds we may want to
    // make this the callers resposibilty to avoid duplicate shows().
    if (status != JOG_STATUS_UNKNOWN) {
        LEDs.show();
    }

    // Since when we're not jogging the AUX_LED may be showing other things we
    // set this to unknown so we can't accedently not call show() when next we become active.
    if (status == JOG_STATUS_INACTIVE) {
        mostRecentlyShownJogStatus = JOG_STATUS_UNKNOWN;
    } else {
        mostRecentlyShownJogStatus = status;
    }
}

void sendIRSensorValue(boolean isResponse, char sensor)
{
    int value =  averagedSensorValue(sensor);
    if (isResponse) {
        if (sensor == SENSOR_LEFT) {
            sendResponseStringBT(String(value));
        } else {
            sendResponseStringBT(String(value));
        }
    } else {
        if (sensor == SENSOR_LEFT) {        
            sendResponseStringBT(String(value));
        } else {
            sendResponseStringBT(String(value));
        }
    }
    Serial.println(String(value).c_str());
}

void sendCarriageStepperPosition()
{
    sendStringBT(String(gCarriageStepper.currentPosition()));
}

void sendRightCamStepperPosition()
{
    sendStringBT(String(gRightCamStepper.currentPosition()));
}

void sendLeftCamStepperPosition()
{
    sendStringBT(String(gLeftCamStepper.currentPosition()));
}

void blePoll()
{
    // Tell the nRF8001 to do whatever it should be working on.
    BTLESerial.pollACI();
    
    //  Ask what is our current status is
    gStatus = BTLESerial.getState();
    
    if (gStatus != gLastStatus) {
        // Send status changes out to the serial port for debugging.
        if (gStatus == ACI_EVT_DEVICE_STARTED) {
            Serial.println(F("* Advertising started"));
            gBTDeviceEverStarted = true;
            setStatusLED(STATUS_NOT_CONNECTED);
        } else if (gStatus == ACI_EVT_CONNECTED) {
            Serial.println(F("* Connected"));
            setStatusLED(STATUS_CONNECTED);
            // TODO: Do something that dumps the state up to the phone at this point so 
            //       people can connect/disconnect and the loom will maintain weaving state.
            // Serial.println(F("Done Sending State"));
        } else if (gStatus == ACI_EVT_DISCONNECTED) {
            Serial.println(F("* Disconnected or advertising timed out"));
            setStatusLED(STATUS_NOT_CONNECTED);
        } else {
            Serial.print(F("* Unknown serial state: "));
            Serial.println(String(gStatus).c_str());
        }
        gLastStatus = gStatus;
    } else if (gStatus == ACI_EVT_DEVICE_STARTED) {
        // When polling in the non connected state we update all the time to
        // let the search animation happen.
        setStatusLED(STATUS_NOT_CONNECTED);
    }
}

unsigned char blockingReadUnsignedChar()
{
    blePoll();  
    while (!(gStatus == ACI_EVT_CONNECTED && BTLESerial.available())) {
        blePoll();
    }
    return BTLESerial.read();
}

// This only returns if there is some character after the digits stuff, and that character 
// gets throw away. We may want to send a \n or something for all commands? TODO: Fix this
long blockingReadLong()
{
    unsigned char digit = blockingReadUnsignedChar();
    long value = 0;
    long postMultiply = (digit == '-') ? -1 : 1;

    // Right now the '-' code doesn't seem to work. Not sure why.
    // The '-' doesn't arrive.  We might be screwing it up on the app end of things.
    if (digit == '-') {
      digit = blockingReadUnsignedChar();
    }

    // Primative a to i for units, etc.
    while (digit >= '0' && digit <= '9') {
      value = value*10 + (digit - '0');
      digit = blockingReadUnsignedChar();
    } 
    return value*postMultiply;
}

// This is the standard getting of a char, but it prints out the char we got.
// It's used by the various command handlers.
char blockingGetChar() 
{
    char commandChar = (char)blockingReadUnsignedChar();

    Serial.print("Got ");
    Serial.println(commandChar);

    return commandChar;
}

void sendStringBT(const String &s)
{
    if (gStatus != ACI_EVT_CONNECTED) {
        return;
    }
    Serial.print(F("Sending: "));
    Serial.println(s);
    if (s.length() > 31) {
        Serial.println("Clipping");
    }
    uint8_t sendbuffer[32];
    s.getBytes(sendbuffer, 32);
    char sendbuffersize = min(32, s.length());
    BTLESerial.write(sendbuffer, sendbuffersize);
    blePoll();
}

void sendResponseStringBT(const String &s)
{
    if (gStatus != ACI_EVT_CONNECTED) {
        return;
    }

    uint8_t sendbuffer[33];
    sendbuffer[0] = '*';
    s.getBytes(&(sendbuffer[1]), 32);

    Serial.print(F("Sending response "));
    Serial.println(s);

    if (s.length() > 32) {
        Serial.println(F("Response is too long and will be randomly clipped down"));
    }
    
    char sendbuffersize = min(33, s.length() + 1);
    BTLESerial.write(sendbuffer, sendbuffersize);
    blePoll();
}

// The stepper motors produce a lot of noise
// so we can do some averaging, but for now this is disabled.
int averagedSensorValue(char sensor)
{
    int i;
    int value = 0;
    for (i=0; i<SENSOR_SAMPLE_SIZE; ++i) {
        if (sensor == SENSOR_LEFT) {
            value += analogRead(LEFT_IR_SENSOR_ANALOG_IN);
        } else {
          value += analogRead(RIGHT_IR_SENSOR_ANALOG_IN);
        }
    }
    return value/SENSOR_SAMPLE_SIZE;
}

int rawSensorValue(char sensor)
{
    if (sensor == SENSOR_LEFT) {
        return analogRead(LEFT_IR_SENSOR_ANALOG_IN);
    } else {
        return analogRead(RIGHT_IR_SENSOR_ANALOG_IN);
    }
}

bool irSensorIsBlocked(char sensor)
{
    return averagedSensorValue(sensor) > IR_SENSOR_THRESHOLD;
}

void waitForBTCommand()
{
    while (!BTLESerial.available()) {
        blePoll();
    }
}

void possiblyNotifyAboutRightLimitSwitch(boolean force)
{
    // We do some debouncing so we must have a certain number
    // of matching values in a row before sending an update.
    // This is mostly left over from optical gap sensor hooked up to 
    // digital inputs  With actual switchs this is overkill, but reduces 
    // noise for the heavy weight act of sending slow BTLE messages.
    static boolean mostRecentlySentState = HIGH;
    static boolean mostRecentState = HIGH;
    static unsigned mostRecentStateCount = 0;
  
    boolean state = digitalRead(LIMIT_SWITCH_RIGHT_DIG_IN);

    if (force) {
        mostRecentlySentState = !state;
        mostRecentStateCount = DEBOUNCE_LIMIT;
    }
  
    if (state != mostRecentState) {
        mostRecentState = state;
        mostRecentStateCount = 0;
    } else {
        if (mostRecentStateCount < DEBOUNCE_LIMIT) {
            mostRecentStateCount++;
        } else {
            if (state != mostRecentlySentState) {
                if (state == LOW) {
                    BTLESerial.print("R0");
                } else {
                    BTLESerial.print("R1");
                }
                mostRecentlySentState = state;
            }
        }
    }
}

void possiblyNotifyAboutLeftLimitSwitch(boolean force)
{
 #if WE_SUPPORT_LEFT_LIMIT_SWITCH
    // We do some debouncing so we must have a certain number
    // of matching values in a row before sending an update.
    static boolean mostRecentlySentState = HIGH;
    static boolean mostRecentState = HIGH;
    static unsigned mostRecentStateCount = 0;
  
    boolean state = digitalRead(LIMIT_SWITCH_LEFT_DIG_IN);

    if (force) {
        mostRecentlySentState = !state;
        mostReceStateCount = DEBOUNCE_LIMIT;
    }
  
    if (state != mostRecentState) {
        mostRecentState = state;
        mostRecentStateCount = 0;
    } else {
        if (mostRecentStateCount < DEBOUNCE_LIMIT) {
            mostRecentStateCount++;
        } else {
            if (state != mostRecentlySentState) {
                if (state == LOW) {
                    BTLESerial.print("L0");
                } else {
                    BTLESerial.print("L1");
                }
                mostRecentlySentState = state;
            }
        }
    }
 #endif
}

void jogCarriageMotor()
{
    boolean done = false;
    unsigned char jog = 128;
    int speed = 0;
    int previousSpeed = 0;
    
    setJogStatusLED(JOG_STATUS_ACTIVE);
  
    blePoll();
      
    // Start out not moving and tell the device about the limit  
    // switch state.
    gCarriageStepper.setSpeed(speed);
    possiblyNotifyAboutRightLimitSwitch(SEND_FORCED);
    possiblyNotifyAboutLeftLimitSwitch(SEND_FORCED);

    float speedF = 0.0;

    while (!done && gStatus == ACI_EVT_CONNECTED) {
        blePoll();
        gCarriageStepper.runSpeed();
    
        while (gStatus == ACI_EVT_CONNECTED && BTLESerial.available()) {
            jog = BTLESerial.read();
            //Serial.println(jog);
            if (jog == JOG_FINISHED_CODE) {
                done = true;
                break;
            }
        }

        // We map to a float range of -1 to 1 and cube it to give us more detail around
        // zero.  Giving us a nice log taper style jog.
        speed = map(jog, 1, 255, -1000, 1000);
        speedF = (float)speed/1000.0;
        speedF = -speedF*speedF*speedF;

        speed = (int)(MOTOR_MAX_SPEED * speedF);
        
        // We still have to be certain that 128 maps to 0 or we could be left exiting the jog
        // with the motor still moving.
        if (jog == JOG_FINISHED_CODE) {
            speed = 0;
        }

        // Clamp the speed so you can only drive away from limits.
        // TODO: Possibly start limiting speed as we approach limits if 
        // we think we know where the limits are.

#if WE_SUPORT_LEFT_LIMIT_SWITCH
        // Not wired by default so we ignore for now.
        if (speed < 0 && digitalRead(LIMIT_SWITCH_LEFT_DIG_IN)) {
            speed = 0;
        }
#endif
        if (speed > 0 && digitalRead(LIMIT_SWITCH_RIGHT_DIG_IN)) {
            speed = 0;
        } 
       
        if (speed != previousSpeed) {
            //Serial.println(speed);
            Serial.println(gCarriageStepper.currentPosition());
            gCarriageStepper.setSpeed(speed);
            previousSpeed = speed;
        }

        possiblyNotifyAboutRightLimitSwitch(SEND_OPTIONAL);
        possiblyNotifyAboutLeftLimitSwitch(SEND_OPTIONAL);

        if (digitalRead(LIMIT_SWITCH_RIGHT_DIG_IN) /* || digitalRead(LIMIT_SWITCH_LEFT_DIG_IN)*/) {
            setJogStatusLED(JOG_STATUS_LIMIT_HIT);
        } else {
            setJogStatusLED(JOG_STATUS_ACTIVE);
        }
        
        //   Serial.print(jog);
    }
    gCarriageStepper.setSpeed(speed);
    setJogStatusLED(JOG_STATUS_INACTIVE);
}

void jogRightCamMotor()
{
    boolean done = false;
    unsigned char jog = 128;
    int speed = 0;
    int previousSpeed = 0;
  
    blePoll();
      
    // Start out not moving and tell the device about the limit  
    // switch state.
    gRightCamStepper.setSpeed(speed);

    while (!done && gStatus == ACI_EVT_CONNECTED) {
        blePoll();
        gRightCamStepper.runSpeed();
    
        while (gStatus == ACI_EVT_CONNECTED && BTLESerial.available()) {
            jog = BTLESerial.read();
            Serial.println(jog);
            if (jog == JOG_FINISHED_CODE) {
                done = true;
                speed = 0;
                break;
            }
        }

        // Use the cube so it is non linear and stays small close to 0. 
        speed = map(jog, 1, 255, 1000, -1000);
        float speedF = (float)speed/1000.0;
        speedF = -speedF*speedF*speedF;

        speed = (int)(MOTOR_MAX_SPEED/4 * speedF);
        
        if (speed != previousSpeed) {
            //    Serial.println(speed);
            gRightCamStepper.setSpeed(speed);
            previousSpeed = speed;
        }

        if (done) {
            gRightCamStepper.setCurrentPosition(0);
        }
    }
    gCarriageStepper.setSpeed(speed);
}

void setCarriageHome()
{
     Serial.println(F("Setting carriage home."));
    // Home is the position where the right cam rotator is visible on the right hand side before
    // it enters the stack of cams, and is two cam widths to the right of the first cam.
    gCarriageStepper.setCurrentPosition(0);
}

// This us like gCarriageStepper.runToNewPosition but always approaches the position from
// the same "from the home end" direction as a simple cheesy way to deal with backlash.
void runCarriageToNewPosition(long targetPosition) {
    // In our current setup Home is position 0 and End is - so adding takes us to the home side of the target.
    if (gCarriageStepper.currentPosition() < targetPosition) {
        gCarriageStepper.runToNewPosition(targetPosition + MOTOR_STEPS_PER_REVOLUTION/8);
    }
    gCarriageStepper.runToNewPosition(targetPosition);
}

long interpolatedMeasuredPosition(int index)
{
    float approximateCamWidth = float(gMeasuredPositions[6] - gMeasuredPositions[0])/59.0;

    // We actually know that the -2 camp position should be 0 because that is were
    // the visible alignment mark is. The way we extrapolate beyond 0->59 makes -2 not be
    // guaranteed to be zero so we handle it directly.
    if (index == -2) {
        return 0;
    }

    // Our measured positions only work in the index range 0->59 on a 60 cam loom
    // so we extrapolate beyond that.  Other than the -2 index these positions have no
    // physical measurement associated with them.
    if (index + 1 < 0) {
        return gMeasuredPositions[0] + long(float(index + 1)*approximateCamWidth);
    }

    if (index >= 60) {
        return  gMeasuredPositions[6] + long(float(index - 59)*approximateCamWidth);
    }

    // If it is in the valid range we compute it by interpolation.
    float floatIndex = float(index + 1)/10.0;
    int lowerIndex = int(floor(floatIndex));
    int upperIndex = int(ceil(floatIndex));

    long position = gMeasuredPositions[lowerIndex];
    if (lowerIndex != upperIndex) {
        float span = gMeasuredPositions[upperIndex] - gMeasuredPositions[lowerIndex];
        position += int(span*(floatIndex - float(lowerIndex)));
    }
    return position;
}

// Indexed as 0 being the right most cam and gInfo.numberOfWarpLayers - 1 being the left most cam.
void seekToWarpLayer(int warpLayerIndex)
{
    if (warpLayerIndex > NUMBER_OF_WARP_LAYERS + 3 || warpLayerIndex < -3) {
        Serial.println(F("Seek index out of range, ignoring"));
        return;
    }
    
    Serial.print(F("Seeking to warp layer "));
    Serial.println(String(warpLayerIndex).c_str());
    
    long targetPosition = interpolatedMeasuredPosition(warpLayerIndex);
    
    runCarriageToNewPosition(targetPosition);
    Serial.print(F("Got to position "));
    Serial.println(String(targetPosition).c_str());
    
    Serial.print(F("computed warp layer index is "));
    Serial.println(String(getWarpLayerIndexFromPosition(gCarriageStepper.currentPosition())).c_str());
}

void setCamState(int warpLayerIndex, char camState) 
{
    // If it's already set to what we want we can do nothing.
    if (gCurrentCamStates[warpLayerIndex] == camState) {
        return;
    }
    seekToWarpLayer(warpLayerIndex);

    // We always overshoot a little and back off because that relieves any 
    // pressure on the cam, and leaves it freer to move left/right.
    long newTarget = gRightCamStepper.targetPosition() + MOTOR_STEPS_PER_REVOLUTION/2;
    gRightCamStepper.runToNewPosition(newTarget + MOTOR_STEPS_PER_REVOLUTION/CAM_OVERSHOOT_ROTATION_DIVISOR);
    gRightCamStepper.runToNewPosition(newTarget);   

    gCurrentCamStates[warpLayerIndex] = camState;
}

long blockingBTReadLongArgAfterSpace()
{
    char nextChar = blockingGetChar();

    // This is pretty horrible to just return 0 when this fails.
    if (nextChar != ' ') {
        Serial.println(F("No space sent after command."));
        return 0;
    }

    long argValue = blockingReadLong();
    return argValue;
}

// This scans the carage toward gWarpLeftEndLimit and keeps stepping until the averaged right sensor value
// goes above the provided threshold. abs(Threshold) can be in the range 1->1022 with positive values 
// for rising edge and negative values for falling edge. 
void readAndScanForwardToIRSensorEdge()
{
    int threshold = (int)blockingBTReadLongArgAfterSpace();
    long position = gCarriageStepper.currentPosition();

    if (threshold < 0) {
        // Do falling edge test.
        while (averagedSensorValue(SENSOR_RIGHT) > -threshold && !digitalRead(LIMIT_SWITCH_RIGHT_DIG_IN)) {
            position += 1;
            gCarriageStepper.runToNewPosition(position); 
        }
    } else {
        // Do rising edge test.
        while (averagedSensorValue(SENSOR_RIGHT) < threshold && !digitalRead(LIMIT_SWITCH_RIGHT_DIG_IN)) {
            position += 1;
            gCarriageStepper.runToNewPosition(position); 
        }
    }
    sendResponseStringBT(String(gCarriageStepper.currentPosition()));
}

void handleCarriageCommands() 
{
     // Serial.println(F("handleCarriageCommands."));
    char commandChar = blockingGetChar();
    if (commandChar == 'j') {  // cj Enter Carriage Jog mode
        // Jog is it's own mode, so we acknowlage the command before going into the mode.
        acknowlageBLE();
        jogCarriageMotor();
    } else if (commandChar == 'h') { // ch mark the current position as carriage home
        setCarriageHome();
        acknowlageBLE();          
    } else if (commandChar == 'r') { // cr moves the carriage right one warp layer.
        // Ack before since these can take a while and the command will time out.
        acknowlageBLE();
        seekToWarpLayer(getWarpLayerIndexFromPosition(gCarriageStepper.currentPosition()) - 1);
    } else if (commandChar == 'l') { // cl moves the carriage left one warp layer.
        // Ack before since these can take a while and the command will time out.
        acknowlageBLE();
        seekToWarpLayer(getWarpLayerIndexFromPosition(gCarriageStepper.currentPosition()) + 1);
    } else if (commandChar == 's') { // cs <long threshold> -> <long> scans carriage toward end looking for rising IR.
        //  This sends its own response string, but if it is too far away this may time out.
        readAndScanForwardToIRSensorEdge();  
    } else {
        sendResponseStringBT(String("Unknown Carriage Command c" + String(commandChar)));
    }
}
    
void handleRightCamCommands() 
{
     // Serial.println(F("handleRightCamCommands."));
    char commandChar = blockingGetChar();
    long newTarget = 0;

    if (commandChar == 'j') { // rj go into carraige jog mode
        Serial.println(F("Jog right heddle cam."));
        acknowlageBLE();                
        jogRightCamMotor();
    } else  if (commandChar == 'c') {  // rc rorate cam counter clockwise
        Serial.println(F("Rotating right cam counter clockwise."));
        acknowlageBLE();
        // Overshoot by a little and return so we are less in contact with the cam and will be
        // free to move left right.
        newTarget = gRightCamStepper.targetPosition() + MOTOR_STEPS_PER_REVOLUTION/2;
        gRightCamStepper.runToNewPosition(newTarget + MOTOR_STEPS_PER_REVOLUTION/CAM_OVERSHOOT_ROTATION_DIVISOR);
        gRightCamStepper.runToNewPosition(newTarget);      
    } else if (commandChar == 'w') {  // rw rotate the cam clockwise.  (counter witershins)
        Serial.println(F("Rotating right cam clockwise."));   
        acknowlageBLE(); 
        newTarget = gRightCamStepper.targetPosition() - MOTOR_STEPS_PER_REVOLUTION/2;
        gRightCamStepper.runToNewPosition(newTarget - MOTOR_STEPS_PER_REVOLUTION/CAM_OVERSHOOT_ROTATION_DIVISOR);
        gRightCamStepper.runToNewPosition(newTarget);
    } else if (commandChar == 's') {  // rs scan the all the camp positions.  This is slow. So it ack's right away.
        acknowlageBLE();
        scanAllCamStates();  // Sends a nc after this is done.  Because new cam state is avaialble.
    }  else {
        sendResponseStringBT(String("Unknown Right Cam Command r" + String(commandChar)));
    }   
}

void readCamStates(unsigned char camStates[])
{
    // The app will send us a sequence of 0 and 1 chars.  0 for "lifts when + lever is pulled" 
    // and 1 for "lifts when - lever is pulled" We expect to have as many as the number of layers we've gotten or we
    // error out.
    char commandChar = blockingGetChar();
    if (commandChar != ' ') {
        Serial.println(F("No space sent after sn command."));
        return;
    }

    // Apply from left to right so the 0's and 1's in the logs are visually in the same order.
    Serial.print(F("Reading cam states for "));
    Serial.print(String(NUMBER_OF_WARP_LAYERS).c_str());
    Serial.println(F(" warp layers"));
    for (int i=NUMBER_OF_WARP_LAYERS - 1; i>=0; --i) {
        commandChar = blockingGetChar();
        if (commandChar == '0') {
            camStates[i] = CAM_STATE_POSITIVE_LEVER;
        } else if (commandChar == '1') {
            camStates[i] = CAM_STATE_NEGITIVE_LEVER;
        } else {
            Serial.println(F("Unknown cam state recieved."));
        }
    }
}

int getWarpLayerIndexFromPosition(long position)
{
    float approximateCamWidth = float(gMeasuredPositions[6] - gMeasuredPositions[0])/59.0;
    int approximateIndex = floor( ((float)position + approximateCamWidth/2.0)/approximateCamWidth) - 2;

    // This search can fail far from the origin, but for operating range positions it should work fine.
    int closestIndex = approximateIndex;
    long minDistance = abs(interpolatedMeasuredPosition(approximateIndex) - position);
    long currentDistance = minDistance;
    for (int i=approximateIndex - 2; i <= approximateIndex + 2; ++i) {
        currentDistance = abs(interpolatedMeasuredPosition(i) - position);
        if (currentDistance < minDistance) {
            closestIndex = i;
            minDistance = currentDistance;
        }
    }
    Serial.print(F("computed warp index "));
    Serial.println(String(closestIndex).c_str());
    return closestIndex;
}

void applyCamStates(unsigned char camStates[])
{
    // If it's close to right (home) end we update from rigth to left.
    if (getWarpLayerIndexFromPosition(gCarriageStepper.currentPosition()) < NUMBER_OF_WARP_LAYERS/2) {
        Serial.println(F("Applying state from right to left."));
        for (int i=0; i<NUMBER_OF_WARP_LAYERS; ++i) {
            setCamState(i, camStates[i]);    
        }
    } else {
        // Otherwise update from left to right.
        Serial.println(F("Applying state from left to right."));
        for (int i=NUMBER_OF_WARP_LAYERS - 1; i>=0; --i) {
            setCamState(i, camStates[i]);    
        }
    }
    notifyAboutNewCamPositionsAvailable();
}

void prepareForPowerOff()
{
    Serial.println(F("Preparing for power off."));
    // Put all the cams in the forward position because that's 
    // what we assume on powerup.
    for (int i=0; i<NUMBER_OF_WARP_LAYERS; ++i) {
        gTargetCamStates[i] = CAM_STATE_POSITIVE_LEVER;
    }
    applyCamStates(gTargetCamStates);
    seekToWarpLayer(-2);
}

void handleSettingCommands() 
{
    //Serial.println(F("handleSettingCommands."));
    char commandChar = blockingGetChar();
    if (commandChar == 'p') {  // sp <ones and zeros one per warp layer>  Set next row of cam states.
        
        // Once we have parsed the state we acknowlage. So the command doesn't time out
        readCamStates(gTargetCamStates);
        acknowlageBLE();       
        applyCamStates(gTargetCamStates);
        // TODO: Have some other way of letting the app UI know that the state has actually been fully updated
        // which can take a long time, and would cause the command to time out.
    } else if (commandChar == 's') {  // ss <Long position> -> Int   Set position and get sensor value.
        // ss set position and send back right sensor averaged value.  There is some danger of 
        // timeout if the moving to the position takes longer than 2 seconds.
        long targetPosition = blockingBTReadLongArgAfterSpace();
        gCarriageStepper.runToNewPosition(targetPosition);
        sendResponseStringBT(String(averagedSensorValue(SENSOR_RIGHT)));    
    } else {
        sendResponseStringBT(String("Unknown Setting Command s" + String(commandChar)));
    }
}

void handleGettingCommands() 
{
    //Serial.println(F("handleGettingCommands."));
    char commandChar = blockingGetChar();
    if (commandChar == 'm') {            // gm -> Int Get number of micro steps.
        // We no longer support dynamic micro step changes, so this is a constant.
        sendResponseStringBT(String((int)MOTOR_MICROSTEPS_PER_STEP));
    } else if (commandChar == 'n') {     // gn -> Int  Get number of warp layers.
        sendResponseStringBT(String(NUMBER_OF_WARP_LAYERS));  
    } else if (commandChar == 's') {     // gs -> Int Get averaged sensor value.
        sendResponseStringBT(String(averagedSensorValue(SENSOR_RIGHT)));  
    } else if (commandChar == 'r') {     // gr -> Int Get raw sensor value.
        sendResponseStringBT(String(rawSensorValue(SENSOR_RIGHT)));
    } else if (commandChar == 'i') {     // gi -> Int Get current warp layer index.
        sendResponseStringBT(String(getWarpLayerIndexFromPosition(gCarriageStepper.currentPosition())));
    } else if (commandChar == 'p') {     // gp -> Long Get current carriage position.
        sendResponseStringBT(String(gCarriageStepper.currentPosition()));
    } else if (commandChar == 'c') {     // gc <waprIndex> -> <Int state> Get cam state. 0 -> forward (+ lever), 1 -> back (- lever), 0 Unknown
        int warpIndex = (int)blockingBTReadLongArgAfterSpace();
        int state = CAM_STATE_UNKNOWN;
        if (warpIndex >= 0 && warpIndex < NUMBER_OF_WARP_LAYERS) {
            state = gCurrentCamStates[warpIndex];
        }
        sendResponseStringBT(String(state)); // ga -> <array of all cams cam states from NUMBER_OF_WARP_LAYERS-1 -> 0> see gc for values.
    } else {
        sendResponseStringBT(String("Unknown Getting Command g" + String(commandChar)));
    }
}

void commandLoop()
{ 
    boolean done = false;
    char commandChar = 0;
  
    while (!done) {
        blePoll();

        // TODO: Figure out when best to acknowlage.
        commandChar = blockingGetChar();        
        if (commandChar == 'g') { // Eventually a bunch of get commands for getting values.
            handleGettingCommands();
        } else if (commandChar == 's') { // Eventually a bunch of set commands for setting values.
            handleSettingCommands();
        } else if (commandChar == 'c') { // Carriage commands cj, ch, ce, cr, cl, cn
            handleCarriageCommands();
        } else if (commandChar == 'r') { // Right cam commands. rj, rc, rw
            handleRightCamCommands();
        } else if (commandChar == 'x') {
            acknowlageBLE();
            prepareForPowerOff();
        } else if (isspace(commandChar)) {
            Serial.println(F("Ignoring white space command.")); 
        } else if (!isalpha(commandChar))  {
            Serial.println(String("Ignoring non alphic command " + String((int)commandChar))); 
        } else {
             sendResponseStringBT(String("Unknown Command " + String(commandChar)));
        }
    }
}

void ledTest()
{
    setStatusLED(STATUS_CONNECTED);
    delay(500);
    setStatusLED(STATUS_ERROR);
    delay(500);
    setStatusLED(STATUS_OFF);
    delay(500);
    setJogStatusLED(JOG_STATUS_ACTIVE);
    delay(500);
    setJogStatusLED(JOG_STATUS_WARN);
    delay(500);
    setJogStatusLED(JOG_STATUS_INACTIVE);
    delay(500);
}

void loop()
{
   //ledTest();
   commandLoop();
}
